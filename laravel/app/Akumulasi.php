<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akumulasi extends Model
{
  protected $fillable = [
      'id','no_kssd','no_trx','tgl_trx','nominal','aktif','admin','gerai','type','created_at','updated_at'
  ];
}
