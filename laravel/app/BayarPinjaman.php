<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BayarPinjaman extends Model
{
  protected $fillable = [
      'id','no_faktur','pinjamen_id','tgl_trx','mata_uang','jenis_pembayaran','jumlah_piutang','bayar','kas_bank','sisa_pinjaman','keterangan','pokok','bunga','aktif','admin','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by'
  ];
}
