<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bukusaldotransaksi extends Model
{
  protected $fillable = [
      'id','user_id','no_anggota','no_trx','tgl_trx','nominal','mutasi','keterangan','saldo','aktif','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by'
  ];
}
