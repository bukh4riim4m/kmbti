<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Daftar extends Model
{
  protected $fillable = [
      'no_anggota','name','tgl_daftar','tpt_lahir','tgl_lahir','nik','npwp','email','agama','pendidikan','pekerjaan','statuskeluarga','nama_ibu','nama_ayah','telp','jenkel','alamat','kelurahan','kecamatan','kabupaten','komunitas','propinsi','pendapatan','pekerjaan','type','pokok','wajib','sukarela','investasi','wakaf','infaq','invoic','fotodiri','fotoktp','status','admin','aktif'
  ];

  public function status_id(){
    return $this->belongsTo('App\Status','status');
  }
}
