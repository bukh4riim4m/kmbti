<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Komunitas;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use App\User;
use App\Simpanan;
use App\Berita;
use App\Simpananadmin;
use App\Akumulasi;
use App\City;
use App\SaldoKasbank;
use App\MutasiKas;
use App\MutasiBank;
use Image;
use Excel;
use DB;
use Log;

class DataAnggotaController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function home()
    {
        $dashboard ="dashboard";
        return view('administrator.index', compact('dashboard'));
    }
    public function tambarberita(Request $request)
    {
        $berita = Berita::create([
        'judul'=>$request->judul,
        'berita'=>$request->berita,
        'admin'=> $request->user()->id,
        'aktif'=>1
      ]);
        if ($berita) {
            flash()->overlay('Berita berhasil di tambahkan.', 'INFO');
            return redirect()->back();
        }
        flash()->overlay('Berita gagal di tambahkan.', 'INFO');
        return redirect()->back();
    }
    public function editberita(Request $request, $id)
    {
        if ($request->action =='edit') {
            $berita = Berita::find($id);
            $berita->judul = $request->judul;
            $berita->berita = $request->berita;
            $berita->admin = $request->user()->id;
            if ($berita->update()) {
                flash()->overlay('Berita berhasil di Edit.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Berita gagal di Edit.', 'INFO');
            return redirect()->back();
        }
        $berita = Berita::find($id);
        $berita->aktif = 0;
        $berita->admin = $request->user()->id;
        if ($berita->update()) {
            flash()->overlay('Berita berhasil di Hapus.', 'INFO');
            return redirect()->back();
        }
        flash()->overlay('Berita gagal di Hapus.', 'INFO');
        return redirect()->back();
    }
    public function dataadmin(Request $request)
    {
      $dashboard ="dataMaster";
      $nomor =$request->no_karyawan;
      $name =$request->name;
      $type="";
      if ($request->action =='tambah') {
        $validatedData = $request->validate([
        'name' => 'required|string|max:255',
        'no_karyawan' => 'required|min:6',
        'id_login' => 'required|min:6',
        'password_login' => 'required|min:6'
        ]);
        if ($idlog = User::where('sequence',$request->id_login)->first()) {
          flash()->overlay('GAGAL, ID Login Sudah di gunakan.','INFO');
          return redirect()->back();
        }
        if ($idemail = User::where('email',$request->email)->first()) {
          flash()->overlay('GAGAL, Email Sudah di gunakan.','INFO');
          return redirect()->back();
        }
        $user = User::create([
          'name' => $request->name,
          'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
          'no_anggota' => $request->no_karyawan,
          'sequence' => $request->id_login,
          'email' => $request->email,
          'telp'=>$request->telp,
          'jenkel' => $request->jenkel,
          'password' => Hash::make($request->password_login),
          'saldo' => 0,
          'admin' => $request->user()->id,
          'aktif' => 1,
          'type' => 'admin'
        ]);
        if ($user) {
          flash()->overlay('Administrator berhasil di tambahkan.','INFO');
          return redirect()->back();
        }
        flash()->overlay('Administrator gagal di tambahkan.','INFO');
        return redirect()->back();
      }elseif ($request->action =='edit') {
        if ($request->fotodiri) {
          $this->validate($request, [
                'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
          ]);
          $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
        }
        DB::beginTransaction();
        try {
          $user = User::find($request->ids);
          $user->name=$request->name;
          $user->no_anggota=$request->no_karyawan;
          $user->tgl_lahir=date('Y-m-d', strtotime($request->tgl_lahir));
          $user->email=$request->email;
          $user->telp=$request->telp;
          $user->jenkel=$request->jenkel;

          if ($request->fotodiri) {
            if ($user->fotodiri =='null' || $user->fotodiri =='') {
              $user->fotodiri=$diri;
              $destination_foto =public_path('foto');
              $request->fotodiri->move($destination_foto, $diri);
            }else {
              $destination_foto =public_path('foto/'.$user->fotodiri);
              if(file_exists($destination_foto)){
                          unlink($destination_foto);
              }
              $user->fotodiri=$diri;
              $destination_foto =public_path('foto');
              $request->fotodiri->move($destination_foto, $diri);
            }
          }
          $user->update();
        } catch (\Exception $e) {
          Log::info('Gagal Edit Profil:'.$e->getMessage());
          DB::rollback();
          flash()->overlay('Gagal Edit Profil.','INFO');
          return redirect()->back();
        }
        DB::commit();
        flash()->overlay('Data Profil berhasil di Edit.','INFO');
        return redirect()->back();
      }elseif ($request->action =='hapus') {
        $delete = User::find($request->ids);
        $delete->aktif = 0;
        if ($delete->update()) {
          flash()->overlay('Data Profil berhasil di Hapus.','INFO');
          return redirect()->back();
        }
        flash()->overlay('Data Profil Gagal di Hapus.','INFO');
        return redirect()->back();
      }
      $user = User::where('no_anggota','LIKE','%'.$nomor.'%')->where('name','LIKE','%'.$name.'%')->where('aktif',1)->where('type','admin')->orderBy('no_anggota','ASC')->get();
      return view('administrator.dataAdministrator',compact('dashboard','user','nomor','name','type'));
    }
    public function datagroup(Request $request)
    {
        $dashboard ="dataMaster";
        $nomor = $request->no_anggota;
        $name = $request->name;
        $type="";
        $user = User::where('aktif', 1)->where('no_anggota', 'LIKE', '%'.$nomor.'%')->where('name', 'LIKE', '%'.$name.'%')->orderBy('no_anggota', 'ASC')->get();
        return view('administrator.dataResetPassword', compact('dashboard', 'user', 'nomor', 'name', 'type'));
    }
    public function carigroup(Request $request)
    {
        $dashboard ="dataMaster";
        $nomor =$request->no_anggota;
        $name =$request->name;
        $type=$request->type;

        $user = User::where('no_anggota', 'LIKE', '%'.$nomor.'%')->where('type', '<>', 'pengurus')->where('name', 'LIKE', '%'.$name.'%')->where('aktif', 1)->get();
        return view('administrator.dataResetPassword', compact('dashboard', 'user', 'nomor', 'name', 'type'));
    }
    public function index(Request $request)
    {
        $dashboard ="dataMaster";
        $user = User::where('aktif', 1)->where('type', 'anggota')->paginate(50);
        $totalsimpanan = User::where('aktif', 1)->where('type', 'anggota')->sum('saldo');
        $totalsaldotransaksi = User::where('aktif', 1)->where('type', 'anggota')->sum('saldotransaksi');
        return view('administrator.dataAnggota', compact('dashboard', 'user', 'totalsimpanan', 'totalsaldotransaksi'));
    }
    public function exportdataanggota(Request $request)
    {
        $user = User::where('aktif', 1)->where('type', '<>', 'karyawan')->get();
        $totalQuery = count($user);
        $while = ceil($totalQuery / 500);
        $collections = collect($user);
        return Excel::create('dataAnggota', function ($excel) use ($while, $collections) {
            for ($i = 1; $i <= $while; $i++) {
                $items = $collections->forPage($i, 500);
                $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                    $sheet->loadView('administrator.report._report_data_anggota', ['user' => $items]);
                });
            }
        })->export('xls');
        return redirect()->back();
    }
    public function caripropinsi(Request $request){
      if ($request->ajax()) {

        $carikabupaten = City::where('province_id',$request->idcari)->get();
        Log::info('ID province = '.$carikabupaten);
        $hasil = "";
        foreach ($carikabupaten as $key => $value) {
          $hasil.="<option value='".$value->id."'>".$value->city_name."</option>";
        }
        return $hasil;
      }
    }
    public function create(Request $request)
    {

        $dashboard ="dataAnggota";
        if ($request->action =='csv') {
            // $validatedData = $request->validate([
            //     'upload' => 'required|max:2048'
            // ]);
            // $ktp = '12345.'.$request->upload->getClientOriginalExtension();
            // $sequence = substr($ktp, -3);
            // if ($sequence !=='csv') {
            //     flash()->overlay('Gagal, Data Upload harus CSV File.', 'INFO');
            //     return redirect()->back();
            // }
            // $upload = $request->file('upload');
            // $filePath = $upload->getRealPath();
            // $file = fopen($filePath, 'r');
            // $header = fgetcsv($file);
            // $escapedHeader=[];
            // //validate
            // foreach ($header as $key => $value) {
            //     $lheader=strtolower($value);
            //     $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
            //     array_push($escapedHeader, $escapedItem);
            // }
            // //looping through othe columns
            // while ($columns=fgetcsv($file)) {
            //     if ($columns[0]=="") {
            //         continue;
            //     }
            //     //trim data
            //     foreach ($columns as $key => &$value) {
            //         $value=$value;
            //     }
            //     // return $columns;
            //     $data = array_combine($escapedHeader, $columns);
            //     // setting type
            //     foreach ($data as $key => &$value) {
            //         $value=($key=="nokssd" || $key=="nama")?(string)$value: (string)$value;
            //     }
            //     // return $data;
            //     // Table update
            //     $nama=$data['nama'];
            //     $nokssd=$data['nokssd'];
            //     $noks=$data['noks'];
            //     $saldos = $data['saldo'];
            //     $username = substr($nokssd, -6);
            //     $budget= User::firstOrNew(['name'=>$nama,'no_anggota'=>$nokssd]);
            //     $budget->no_anggota=$nokssd;
            //     $budget->no_ks212 = $noks;
            //     $budget->name=$nama;
            //     $budget->type='anggota';
            //     $budget->sequence=$username;
            //     $budget->aktif=1;
            //     $budget->saldo=$saldos;
            //     $budget->password=Hash::make($username);
            //     $budget->admin = $request->user()->id;
            //     $budget->save();
            // }
            // flash()->overlay('CSV Berhasil di Upload.', 'INFO');
            // return redirect()->back();
        }
        $type = $request->types;
        //Cek urut anggota
        if ($noanggota = User::orderBy('no_anggota', 'DESC')->where('type','anggota')->first()) {

          $nomor = (int)$noanggota->no_anggota+1;
          $sequence = substr($nomor, -6);
          $noanggota = date('Ym').$sequence;
        }else {
          $noanggota = date('Ym000001');
        }



        ////
        // $noanggota = $request->no_anggota;
        $sequence = substr($noanggota, -6);
        if ($request->tgl_lahir =="") {
            $tanggal_lahir = date('1900-01-01');
        } else {
            $tanggal_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
        }


        if ($request->action =='edit') {

            DB::beginTransaction();
            try {
                $user = User::find($request->ids);
                if ($request->fotodiri) {
                  $this->validate($request, [
                        'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                  ]);
                  $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
                  if ($user->fotodiri =='null' || $user->fotodiri =='') {
                    $user->fotodiri=$diri;
                    $destination_foto =public_path('foto');
                    $request->fotodiri->move($destination_foto, $diri);
                  }else {
                    $destination_foto =public_path('foto/'.$user->fotodiri);
                    if(file_exists($destination_foto)){
                                unlink($destination_foto);
                    }
                    $user->fotodiri=$diri;
                    $destination_foto =public_path('foto');
                    $request->fotodiri->move($destination_foto, $diri);
                  }
                }

                if ($request->fotoktp) {
                  $this->validate($request, [
                        'fotoktp' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                  ]);
                  $ktp = date('YmdHis').'.'.$request->fotoktp->getClientOriginalExtension();
                  if ($user->fotoktp =='null' || $user->fotoktp =='') {
                    $user->fotoktp=$ktp;
                    $destination_foto =public_path('fotoktp');
                    $request->fotoktp->move($destination_foto, $ktp);
                  }else {
                    $destination_foto =public_path('fotoktp/'.$user->fotoktp);
                    if(file_exists($destination_foto)){
                                unlink($destination_foto);
                    }
                    $user->fotodiri=$ktp;
                    $destination_foto =public_path('fotoktp');
                    $request->fotoktp->move($destination_foto, $ktp);
                  }
                }
                $user->name = $request->name;
                $user->tgl_daftar=date('Y-m-d');
                $user->tpt_lahir=$request->tpt_lahir;
                $user->tgl_lahir=$tanggal_lahir;
                $user->nik=$request->nik;
                $user->no_ks212 = $request->no_ks212;
                $user->npwp=$request->npwp;
                $user->email=$request->email;
                $user->agama=$request->agama;
                $user->pendidikan=$request->pendidikan;
                $user->statuskeluarga=$request->statuskeluarga;
                $user->nama_ibu=$request->nama_ibu;
                $user->nama_ayah=$request->nama_ayah;
                $user->telp=$request->telp;
                $user->jenkel=$request->jenkel;
                $user->alamat=$request->alamat;
                $user->kelurahan=$request->kelurahan;
                $user->kecamatan=$request->kecamatan;
                $user->kabupaten=$request->kabupaten;
                $user->komunitas=$request->komunitas;
                $user->propinsi=$request->propinsi;
                $user->pendapatan=$request->pendapatan;
                $user->pekerjaan=$request->pekerjaan;
                $user->saldo=$request->saldo;
                $user->update();

            } catch (\Exception $e) {
                Log::info('Gagal Edit Data Anggota:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Gagal Edit Data Anggota.', 'INFO');
                $user = User::where('aktif', 1)->where('type', 'anggota')->paginate(50);
                $totalsimpanan = User::where('aktif', 1)->where('type', 'anggota')->sum('saldo');
                $totalsaldotransaksi = User::where('aktif', 1)->where('type', 'anggota')->sum('saldotransaksi');
                return view('administrator.dataAnggota', compact('dashboard', 'user', 'totalsimpanan', 'totalsaldotransaksi'));
            }
            DB::commit();
            flash()->overlay('Data Anggota berhasil di Edit.', 'INFO');
            // return redirect()->back();
            $totalsaldotransaksi = User::where('aktif', 1)->where('type', 'anggota')->sum('saldotransaksi');
            $totalsimpanan = User::where('aktif', 1)->where('type', 'anggota')->sum('saldo');
            $user = User::where('aktif', 1)->where('type', 'anggota')->paginate(50);
            return view('administrator.dataAnggota', compact('dashboard', 'user','totalsimpanan','totalsaldotransaksi'));
        } elseif ($request->action == 'cari') {
            $user = User::where('no_anggota', 'LIKE', '%'.$request->no_anggota.'%')->where('name', 'LIKE', '%'.$request->name.'%')->where('aktif', 1)->where('type', 'anggota')->paginate(50);
            $totalsimpanan = User::where('aktif', 1)->where('type', 'anggota')->sum('saldo');
            $totalsaldotransaksi = User::where('aktif', 1)->where('type', 'anggota')->sum('saldotransaksi');
            return view('administrator.dataAnggota', compact('dashboard', 'user', 'totalsimpanan', 'totalsaldotransaksi'));
        } else {
          // return $noanggota;
          $this->validate($request, [
              'fotodiri' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'fotoktp' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
          ]);


          DB::beginTransaction();
            // return $request->all();
            try {
              $diri = $request->file('fotodiri');
              $image = $request->file('fotoktp');
              $imageDiri = $diri->getClientOriginalName();
              $imageName = $image->getClientOriginalName();
              $fileDiri = date('YmdHis')."_".$imageDiri;
              $fileName = date('YmdHis')."_".$imageName;
              $directorydiri = public_path('/foto/');
              $directory = public_path('/fotoktp/');
              $imageUrlDiri = $directorydiri.$fileDiri;
              $imageUrl = $directory.$fileName;
              Image::make($image)->resize(600, 400)->save($imageUrl);
              Image::make($diri)->resize(300, 400)->save($imageUrlDiri);
              //$users->fotoktp = $fileName;

                $user = User::create([
                'tgl_daftar'=>date('Y-m-d'),
                'name'=>$request->name,
                'kerjaan'=>$request->kerjaan,
                'tgl_lahir'=>$tanggal_lahir,
                'nik'=>$request->nik,
                'no_anggota'=>$noanggota,
                'sequence'=>$sequence,
                'npwp'=>$request->npwp,
                'email'=>$request->email,
                'agama'=>$request->agama,
                'pendidikan'=>$request->pendidikan,
                'statuskeluarga'=>$request->statuskeluarga,
                'nama_ibu'=>$request->nama_ibu,
                'nama_ayah'=>$request->nama_ayah,
                'telp'=>$request->telp,
                'jenkel'=>$request->jenkel,
                'alamat'=>$request->alamat,
                'kelurahan'=>$request->kelurahan,
                'kecamatan'=>$request->kecamatan,
                'kabupaten'=>$request->kabupaten,
                'komunitas'=>$request->komunitas,
                'propinsi'=>$request->propinsi,
                'pendapatan'=>$request->pendapatan,
                'pekerjaan'=>$request->pekerjaan,
                'password'=>Hash::make($sequence),
                'type'=>'anggota',
                'fotoktp'=>$fileName,
                'fotodiri'=>$fileDiri,
                'admin'=>$request->user()->id,
                'aktif'=>1,
                'saldo'=>0
              ]);

            } catch (\Exception $e) {
                Log::info('Gagal input Anggota:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Data gagal di tambahkan.', 'INFO');
                $user = User::where('aktif', 1)->where('type', 'anggota')->paginate(50);
                $totalsimpanan = User::where('aktif', 1)->where('type', 'anggota')->sum('saldo');
                $totalsaldotransaksi = User::where('aktif', 1)->where('mutasi','Kredit')->where('type', 'anggota')->sum('saldotransaksi');
                return view('administrator.dataAnggota', compact('dashboard', 'user', 'totalsaldotransaksi'));
            }
            DB::commit();
            flash()->overlay('Data berhasil di Tambahkan.', 'INFO');
            $user = User::where('aktif', 1)->where('type', 'anggota')->paginate(50);
            $totalsimpanan = User::where('aktif', 1)->where('type', 'anggota')->sum('saldo');
            $totalsaldotransaksi = User::where('aktif', 1)->where('type', 'anggota')->sum('saldotransaksi');
            return view('administrator.dataAnggota', compact('dashboard', 'user', 'totalsaldotransaksi'));
        }
    }
    public function detailanggota(Request $request, $id)
    {
        $dashboard ="detailAnggota";
        $active = "pokok";
        $user = User::find($id);
        $pokok = Simpanan::where('jenis_simpanan', 1)->get();
        return view('administrator.detailAnggota', compact('dashboard', 'active', 'user', 'pokok'));
    }

    public function createsimpanan(Request $request)
    {
        $dashboard ="dataSimpanan";
        $from = date('01-m-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d');
        if ($users = User::where('no_anggota', $request->no_anggota)->where('type','anggota')->first()) {
          if ($request->jenis_simpanan ==2) {
          $setor = date('Y-m', strtotime($request->tgl_setor));
            if ($saldo = Simpananadmin::where('no_anggota',$users->no_anggota)->where('tgl_setor', 'LIKE', '%'.$setor.'%')->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'DESC')->first()) {
              flash()->overlay('Simpanan sudah ada bulan ini.', 'INFO GAGAL');
              return redirect()->back();
            }
          }

            // if ($saldo = Simpananadmin::where('no_anggota',$users->no_anggota)->where('jenis_simpanan', 2)->where('aktif', 1)->whereBetWeen('tgl_setor', [$dari,$ke])->orderBy('id', 'DESC')->first()) {
            //   flash()->overlay('Simpanan Wajib sudah ada bulan ini.', 'INFO GAGAL');
            //   return redirect()->back();
            // }
            if ($saldo = Simpananadmin::where('no_anggota',$users->no_anggota)->where('jenis_simpanan', $request->jenis_simpanan)->where('aktif', 1)->orderBy('id', 'DESC')->first()) {
                $saldoakhir = $saldo->saldo;
            } else {
                $saldoakhir = 0;
            }
            $nominals= str_replace(".", "", $request->nominal);
            // if ($request->mutasi =='Kredit') {
            $mutasi = 'Kredit';
            $saldo_akhir = $saldoakhir + $nominals;
            $nominal = $users->saldo + $nominals;
            // } else {
            //     $mutasi = 'Debet';
            //     $saldo_akhir = $saldoakhir - $nominals;
            //     $nominal = $users->saldo - $nominals;
            // }
            if ($request->kasbank=="Kas") {
              DB::beginTransaction();
              try {
                  $user = User::find($users->id);
                  $simpan = Simpananadmin::create([
                    'name'=>$users->name,
                    'no_trx'=>date('ymdHis'),
                    'no_anggota'=>$user->no_anggota,
                    'tgl_setor'=>date('Y-m-d', strtotime($request->tgl_setor)),
                    'jenis_simpanan'=>$request->jenis_simpanan,
                    'nominal'=>$nominals,
                    'mutasi'=>$mutasi,
                    'kasbank'=>$request->kasbank,
                    'ket'=>$request->ket,
                    'saldo'=>$saldo_akhir,
                    'jth_tempo'=>date('Y-m-d'),
                    'awal'=>0,
                    'aktif'=>1,
                    'petugas'=>$request->user()->id
                  ]);
                  $user->saldo = $nominal;
                  $saldos = SaldoKasbank::find(1);

                  $mutasi = MutasiKas::create([
                    'name'=>$users->name,
                    'no_trx'=>date('ymdHis'),
                    'no_anggota'=>$user->no_anggota,
                    'tgl_setor'=>date('Y-m-d', strtotime($request->tgl_setor)),
                    'jenis_simpanan'=>$request->jenis_simpanan,
                    'nominal'=>$nominals,
                    'mutasi'=>$mutasi,
                    'kasbank'=>$request->kasbank,
                    'ket'=>$request->ket,
                    'saldo'=>$saldos->saldo + $nominals,
                    'jth_tempo'=>date('Y-m-d'),
                    'awal'=>0,
                    'aktif'=>1,
                    'petugas'=>$request->user()->id
                  ]);
                  $saldos->saldo = $saldos->saldo + $nominals;
                  $saldos->update();
                  $user->save();

              } catch (\Exception $e) {
                  Log::info('Gagal input Simpanan kas:'.$e->getMessage());
                  DB::rollback();
                  flash()->overlay('Simpanan gagal di tambahkan.', 'INFO');
                  return redirect()->back();
              }
              DB::commit();
              $tahun = date('Y');
              $url = route('admin-history-simpanan');
              $token = csrf_token();
              flash()->overlay("Simpanan berhasil di Tambahkan.<br><br><form action=$url method='POST'><input type='hidden' name='_token' value=$token /><input type='hidden' name='action' value='cari'><input type='hidden' name='dari' value=$tahun /><input type='hidden' name='sampai' value=$tahun /><input type='hidden' name='jenis_simpanan' value=$request->jenis_simpanan /><input type='hidden' name='no_anggota' value=$user->no_anggota /> <button type='submit' class='btn btn-success'>CEK HISTORY</button></form>", "INFO");
              return redirect()->back();
            }else {
              DB::beginTransaction();
              try {
                  $user = User::find($users->id);
                  $simpan = Simpananadmin::create([
                    'name'=>$users->name,
                    'no_trx'=>date('ymdHis'),
                    'no_anggota'=>$user->no_anggota,
                    'tgl_setor'=>date('Y-m-d', strtotime($request->tgl_setor)),
                    'jenis_simpanan'=>$request->jenis_simpanan,
                    'nominal'=>$nominals,
                    'mutasi'=>$mutasi,
                    'kasbank'=>$request->kasbank,
                    'ket'=>$request->ket,
                    'saldo'=>$saldo_akhir,
                    'jth_tempo'=>date('Y-m-d'),
                    'awal'=>0,
                    'aktif'=>1,
                    'petugas'=>$request->user()->id
                  ]);
                  $user->saldo = $nominal;
                  $saldos = SaldoKasbank::find(2);

                  $mutasi = MutasiBank::create([
                    'name'=>$users->name,
                    'no_trx'=>date('ymdHis'),
                    'no_anggota'=>$user->no_anggota,
                    'tgl_setor'=>date('Y-m-d', strtotime($request->tgl_setor)),
                    'jenis_simpanan'=>$request->jenis_simpanan,
                    'nominal'=>$nominals,
                    'mutasi'=>$mutasi,
                    'kasbank'=>$request->kasbank,
                    'ket'=>$request->ket,
                    'saldo'=>$saldos->saldo + $nominals,
                    'jth_tempo'=>date('Y-m-d'),
                    'awal'=>0,
                    'aktif'=>1,
                    'petugas'=>$request->user()->id
                  ]);
                  $saldos->saldo = $saldos->saldo + $nominals;
                  $saldos->update();
                  $user->save();

              } catch (\Exception $e) {
                  Log::info('Gagal input Simpanan bank:'.$e->getMessage());
                  DB::rollback();
                  flash()->overlay('Simpanan gagal di tambahkan.', 'INFO');
                  return redirect()->back();
              }
              DB::commit();
              $tahun = date('Y');
              $url = route('admin-history-simpanan');
              $token = csrf_token();
              flash()->overlay("Simpanan berhasil di Tambahkan.<br><br><form action=$url method='POST'><input type='hidden' name='_token' value=$token /><input type='hidden' name='action' value='cari'><input type='hidden' name='dari' value=$tahun /><input type='hidden' name='sampai' value=$tahun /><input type='hidden' name='jenis_simpanan' value=$request->jenis_simpanan /><input type='hidden' name='no_anggota' value=$user->no_anggota /> <button type='submit' class='btn btn-success'>CEK HISTORY</button></form>", "INFO");
              return redirect()->back();
            }

        }
        flash()->overlay('Nomor Anggota Salah.', 'INFO');
        return redirect()->back();
    }
    public function tambahsimpanan(Request $request)
    {
        $dashboard ="bukuSaldo";
        $jns = $request->jenis_simpanan;
        Log::info('Ini jenis simpanan 1:'.$jns);
        if ($users = User::where('no_anggota', $request->no_anggota)->where('type', '<>', 'karyawan')->first()) {
            if ($saldo = Simpananadmin::where('no_anggota', $users->no_anggota)->where('jenis_simpanan', $request->jenis_simpanan)->where('aktif', 1)->orderBy('id', 'DESC')->first()) {
                $saldoakhir = $saldo->saldo;
            } else {
                $saldoakhir = 0;
            }
            if ($request->mutasi =='Kredit') {
                $mutasi = 'Kredit';
                $saldo_akhir = $saldoakhir + $request->nominal;
                $nominal = $users->saldo + $request->nominal;
            } else {
                $mutasi = 'Debet';
                $saldo_akhir = $saldoakhir - $request->nominal;
                $nominal = $users->saldo - $request->nominal;
            }
            if ($cariawal = Simpananadmin::where('no_anggota', $users->no_anggota)->where('jenis_simpanan', 2)->where('aktif', 1)->where('mutasi', 'Kredit')->where('awal', 1)->first()) {
                $awal = 0;
                $tempo = date('-m-d', strtotime($cariawal->tgl_setor));
            } else {
                $awal = 1;
                $tempo = date('-m-d', strtotime($request->tgl_setor));
            }
            $tanggal = 2019;
            $jns = $request->jenis_simpanan;
            DB::beginTransaction();
            try {
                $user = User::find($users->id);
                $simpan = Simpananadmin::create([
                  'name'=>$users->name,
                  'no_trx'=>date('ymdHis'),
                  'no_anggota'=>$user->no_anggota,
                  'tgl_setor'=>date('Y-m-d', strtotime($request->tgl_setor)),
                  'jth_tempo'=>$tanggal.$tempo,
                  'jenis_simpanan'=>$request->jenis_simpanan,
                  'nominal'=>$request->nominal,
                  'mutasi'=>$mutasi,
                  'ket'=>$request->jenis_simpanan,
                  'saldo'=>$saldo_akhir,
                  'aktif'=>1,
                  'awal'=>$awal,
                  'petugas'=>$request->user()->id
                ]);
                $user->saldo = $nominal;
                $user->save();
            } catch (\Exception $e) {
                Log::info('Gagal input Simpanan:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Simpanan gagal di Simpan.', 'INFO');
                $anggota = $request->no_anggota;
                $cari = $request->action;
                $from =$request->dari;
                $until =$request->sampai;
                $dari = date('Y-m-d', strtotime($from));
                $ke = date('Y-m-d', strtotime($until));
                $users = User::where('no_anggota', 'LIKE', '%'.$anggota.'%')->where('type', 'anggota')->first();
                $simpananpokok = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 1)->where('aktif', 1)->orderBy('id', 'ASC')->get();
                $simpananwajib = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'ASC')->get();
                $simpanansukarela = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 3)->where('aktif', 1)->orderBy('id', 'ASC')->get();
                $simpananinvestasi = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 4)->where('aktif', 1)->orderBy('id', 'ASC')->get();
                return view('administrator.bukuSaldo', compact('dashboard', 'anggota', 'from', 'until', 'users', 'simpananpokok', 'simpananwajib', 'simpanansukarela', 'cari', 'jns'));
            }
            DB::commit();
            flash()->overlay('Simpanan berhasil di Simpan.', 'INFO');
            $jns = $request->jenis_simpanan;
            Log::info('Ini jenis simpanan:'.$jns);
            $anggota = $request->no_anggota;
            $cari = $request->action;
            $from =$request->dari;
            $until =$request->sampai;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($until));
            $users = User::where('no_anggota', 'LIKE', '%'.$anggota.'%')->where('type', 'anggota')->first();
            $simpananpokok = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 1)->where('aktif', 1)->orderBy('id', 'ASC')->get();
            $simpananwajib = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'ASC')->get();
            $simpanansukarela = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 3)->where('aktif', 1)->orderBy('id', 'ASC')->get();
            $simpananinvestasi = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 4)->where('aktif', 1)->orderBy('id', 'ASC')->get();
            return view('administrator.bukuSaldo', compact('dashboard', 'anggota', 'from', 'until', 'users', 'simpananpokok', 'simpananwajib', 'simpanansukarela', 'simpananinvestasi', 'cari', 'jns'));
            return redirect()->back();
        }
    }
    public function exporsimpananadmin(Request $request)
    {
        if ($request->export =='1') {
            $nomor = $request->no_anggota;
            $jenissim = $request->jenis_simpanan;
            $from = $request->dari;
            $to = $request->sampai;
            $mutasis = $request->mutasi;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
            $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 'LIKE', '%'.$jenissim.'%')->where('mutasi', 'LIKE', '%'.$mutasis.'%')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
            $totalQuery = count($simpanan);
            $while = ceil($totalQuery / 500);
            $collections = collect($simpanan);
            return Excel::create($from.' - '.$to, function ($excel) use ($while, $collections) {
                for ($i = 1; $i <= $while; $i++) {
                    $items = $collections->forPage($i, 500);
                    $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                        $sheet->loadView('administrator.report._report_simpanan_anggota', ['simpanan' => $items]);
                    });
                }
            })->export('xls');
        }
    }
    public function editsimpanan(Request $request, $id)
    {
        $dashboard ="bukuSaldo";
        if ($request->action =='edit') {
            // code...
        }
    }
    public function mutasi(Request $request)
    {
        $dashboard ="bukuSaldo";
        $anggota = $request->no_anggota;
        $cari = $request->action;
        $jns = 1;
        $from = date('Y');
        $until = date('Y');
        // $dari = date('Y', strtotime($from));
        // $ke = date('Y', strtotime($until));
        $users = User::where('no_anggota', $anggota)->where('type', '<>', 'karyawan')->first();
        if ($request->action =='cari') {
            $anggota = $request->no_anggota;
            $from =$request->dari;
            $until =$request->sampai;
            // $dari = date('Y', strtotime($from));
            // $ke = date('Y', strtotime($until));

            if (!User::where('no_anggota', 'LIKE', '%'.$anggota.'%')->where('type', 'anggota')->first()) {
                flash()->overlay('Nomor Anggota Tidak Terdaftar.', 'INFO');
                return redirect()->back();
            }
            $jns = $request->jenis_simpanan;
            $users = User::where('no_anggota', 'LIKE', '%'.$anggota.'%')->where('type','anggota')->first();
            $anggota = $users->no_anggota;
        } elseif ($request->action =='edit') {
            $jns = $request->jenis_simpanan;
            $from =$request->dari;
            $until =$request->sampai;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($until));
            DB::beginTransaction();
            $simpanan = Simpananadmin::find($request->ids);
            if ($simpanan->mutasi == 'Kredit') {
                $nilai = -$simpanan->nominal;
            } else {
                $nilai = $simpanan->nominal;
            }

            $cari = Simpananadmin::where('no_anggota', $anggota)->where('jenis_simpanan', $request->jenis_simpanan)->where('aktif', 1)->orderBy('id', 'DESC')->paginate(2);
            $lastDeposit=null;
            foreach ($cari as $key => $caries) {
                ($key!==0)?$lastDeposit=$caries->saldo:$lastDeposit=$lastDeposit;
            }
            // return $lastDeposit;
            if ($request->mutasi == 'Kredit') {
                $nominal = $request->nominal;
                $saldo = $lastDeposit + $request->nominal;
            } else {
                $nominal = -$request->nominal;
                $saldo = $lastDeposit - $request->nominal;
            }
            try {
                $simpanan = Simpananadmin::find($request->ids);
                $simpanan->mutasi = $request->mutasi;
                $simpanan->nominal = $request->nominal;

                $simpanan->saldo = $saldo;
                $simpanan->update();

                $users = User::where('no_anggota', $anggota)->where('type', '<>', 'karyawan')->first();
                $users->saldo = $users->saldo + $nilai;
                $users->update();

                $users = User::where('no_anggota', $anggota)->where('type', '<>', 'karyawan')->first();
                $users->saldo = $users->saldo + $nominal;
                $users->update();
            } catch (\Exception $e) {
                DB::rollback();
                flash()->overlay('Simpanan gagal di Simpan.', 'INFO');
            }
            DB::commit();
            flash()->overlay('Simpanan berhasil di Simpan.', 'INFO');
        } elseif ($request->action =='hapus') {
            $from =$request->dari;
            $until =$request->sampai;
            // $dari = date('Y-m-d', strtotime($from));
            // $ke = date('Y-m-d', strtotime($until));
            DB::beginTransaction();
            $simpanan = Simpananadmin::find($request->ids);
            $jns = $simpanan->jenis_simpanan;
            if ($simpanan->mutasi == 'Kredit') {
                $nilai = -$simpanan->nominal;
            } else {
                $nilai = $simpanan->nominal;
            }
            try {
                $hapus = Simpananadmin::find($request->ids);
                $hapus->aktif = 0;
                $hapus->saldo = 0;
                $hapus->update();

                $users = User::where('no_anggota', $anggota)->where('type', '<>', 'karyawan')->first();
                $users->saldo = $users->saldo + $nilai;
                $users->update();
            } catch (\Exception $e) {
                DB::rollback();
                flash()->overlay('Simpanan gagal di Hapus.', 'INFO');
            }
            DB::commit();
            flash()->overlay('Simpanan berhasil di Hapus.', 'INFO');
        }
        $jns = $request->jenis_simpanan;
        $simpananpokok = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 1)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananwajib = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpanansukarela = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 3)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananinvestasi = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 4)->where('aktif', 1)->orderBy('id', 'ASC')->get();

        return view('administrator.bukuSaldo', compact('dashboard', 'anggota', 'from', 'until', 'users', 'simpananpokok', 'simpananwajib', 'simpanansukarela', 'simpananinvestasi','cari', 'jns'));
    }
    public function akumulasiadmin(Request $request)
    {
        $dashboard ="akumulasi";
        $from = date('01-01-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        $no_kssd = $request->no_kssd;
        $type = $request->type;
        if ($request->action =='cari') {
            $type = $request->type;
            $from = $request->from;
            $to = $request->to;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
        } elseif ($request->action =='edit') {
            if ($users=User::where('no_anggota', 'LIKE', '%'.$no_kssd.'%')->where('aktif', 1)->where('type', '<>', 'karyawan')->first()) {
                $akumulasi = Akumulasi::find($request->ids);
                $akumulasi->no_kssd = $users->no_anggota;
                $akumulasi->tgl_trx = date('Y-m-d', strtotime($request->tgl_trx));
                $akumulasi->nominal = $request->nominal;
                $akumulasi->gerai = $request->gerai;
                $akumulasi->admin = $request->user()->id;
                if ($akumulasi->update()) {
                    flash()->overlay('Data berhasil di simpan.', 'INFO');
                    return redirect()->back();
                }
                flash()->overlay('Data gagal di simpan.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Nomor Anggota Salah.', 'INFO');
            return redirect()->back();
        } elseif ($request->action =='hapus') {
            if ($users=User::where('no_anggota', 'LIKE', '%'.$no_kssd.'%')->where('aktif', 1)->where('type', '<>', 'karyawan')->first()) {
                $akumulasi = Akumulasi::find($request->ids);
                $akumulasi->aktif = 0;
                if ($akumulasi->update()) {
                    flash()->overlay('Data berhasil di Hapus.', 'INFO');
                    return redirect()->back();
                }
                flash()->overlay('Data gagal di Hapus.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Nomor Anggota Salah.', 'INFO');
            return redirect()->back();
        } elseif ($request->action =='tambah') {
            if ($users=User::where('no_anggota', 'LIKE', '%'.$request->no_kssd.'%')->where('aktif', 1)->where('type', '<>', 'karyawan')->first()) {
                $tgl_trx = date('Y-m-d', strtotime($request->tgl_trx));
                $akumulasis = Akumulasi::create([
            'no_kssd'=>$users->no_anggota,
            'no_trx'=>date('ymdhis'),
            'tgl_trx'=>$tgl_trx,
            'nominal'=>$request->nominal,
            'aktif'=>1,
            'gerai'=>$request->gerai,
            'type'=>$request->type,
            'admin'=>$request->user()->id
          ]);
                if ($akumulasis) {
                    flash()->overlay('Data berhasil di simpan.', 'INFO');
                    return redirect()->back();
                }
                flash()->overlay('Data gagal di simpan.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Nomor Anggota Tidak Terdaftar.', 'INFO');
            return redirect()->back();
        } elseif ($request->action =='export') {
            $from = $request->from;
            $to = $request->to;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
            $akumulasis = Akumulasi::whereBetWeen('tgl_trx', [$dari,$ke])->where('aktif', 1)->where('no_kssd', 'LIKE', '%'.$no_kssd.'%')->orderBy('id', 'ASC')->get();
            if ($request->export =='1' && count($akumulasis)>1) {
                $totalQuery = count($akumulasis);
                $while = ceil($totalQuery / 500);
                $collections = collect($akumulasis);
                return Excel::create($from.' - '.$to, function ($excel) use ($while, $collections) {
                    for ($i = 1; $i <= $while; $i++) {
                        $items = $collections->forPage($i, 500);
                        $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                            $sheet->loadView('administrator.report._report_excel_akumulasi', ['akumulasis' => $items]);
                        });
                    }
                })->export('xls');
            }
        } elseif ($request->action =='csv') {
            $validatedData = $request->validate([
        'upload' => 'required'
        ]);
            $ktp = '12345.'.$request->upload->getClientOriginalExtension();
            $sequence = substr($ktp, -3);
            if ($sequence !=='csv') {
                flash()->overlay('Gagal, Data Upload harus CSV File.', 'INFO');
                return redirect()->back();
            }
            $upload = $request->file('upload');
            $filePath = $upload->getRealPath();
            $file = fopen($filePath, 'r');
            $header = fgetcsv($file);
            $escapedHeader=[];
            //validate
            foreach ($header as $key => $value) {
                $lheader=strtolower($value);
                $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
                array_push($escapedHeader, $escapedItem);
            }
            //looping through othe columns
            while ($columns=fgetcsv($file)) {
                if ($columns[0]=="") {
                    continue;
                }
                //trim data
                foreach ($columns as $key => &$value) {
                    $value=$value;
                }
                // return $columns;
                $data = array_combine($escapedHeader, $columns);
                // setting type
                foreach ($data as $key => &$value) {
                    $value=($key=="nokssd" || $key=="tanggal" || $key=="notransaksi")?(string)$value: (string)$value;
                }
                // return $data;
                // Table update
                $nokssds=$data['nokssd'];
                if ($nokssds == 201701000000) {
                    $type = 'Non';
                } else {
                    $type = 'Anggota';
                }
                $notrxs=$data['notransaksi'];
                $tanggals=date('Y-m-d', strtotime($data['tanggal']));
                $nominals=$data['nominal'];
                $gelais = $data['gerai'];
                $simpanan = Akumulasi::firstOrNew(['no_kssd'=>$nokssds,'no_trx'=>$notrxs]);
                $simpanan->no_kssd=$nokssds;
                $simpanan->tgl_trx = $tanggals;
                $simpanan->no_trx = $notrxs;
                $simpanan->nominal=$nominals;
                $simpanan->gerai=$gelais;
                $simpanan->aktif=1;
                $simpanan->type = $type;
                $simpanan->admin = $request->user()->id;
                $simpanan->save();
            }
            flash()->overlay('CSV Berhasil di Upload.', 'INFO');
            return redirect()->back();
        }
        $akumulasis = Akumulasi::whereBetWeen('tgl_trx', [$dari,$ke])->where('aktif', 1)->where('no_kssd', 'LIKE', '%'.$no_kssd.'%')->where('type', 'LIKE', '%'.$type.'%')->orderBy('id', 'ASC')->get();
        return view('administrator.dataAkumulasi', compact('dashboard', 'akumulasis', 'from', 'to', 'no_kssd', 'type'));
    }
    public function saldoakumulasiadmin(Request $request)
    {
        $dashboard ="akumulasi";
        $nomor = $request->no_anggota;
        $tahun = date('Y');
        if ($request->action =='cari') {
            $tahun = $request->tahun;
            if (!$users = User::where('aktif', 1)->where('no_anggota', 'LIKE', '%'.$nomor.'%')->where('type', '<>', 'karyawan')->first()) {
                flash()->overlay('Nomor tidak Terdaftar.', 'INFO');
                return redirect()->back();
            }
            $datas = Akumulasi::where('no_kssd', 'LIKE', '%'.$nomor.'%')->where('aktif', 1)->where('tgl_trx', 'LIKE', '%'.$tahun.'%')->orderBy('id', 'ASC')->get();
        } elseif ($request->action =='edit') {
            $tahun = $request->tahun;
            if (!$users = User::where('aktif', 1)->where('no_anggota', 'LIKE', '%'.$nomor.'%')->where('type', '<>', 'karyawan')->first()) {
                flash()->overlay('Nomor tidak Terdaftar.', 'INFO');
                return redirect()->back();
            }
            $data = Akumulasi::find($request->ids);
            $data->no_kssd = $nomor;
            $data->tgl_trx = date('Y-m-m', strtotime($request->tgl_trx));
            $data->nominal = $request->nominal;
            $data->gerai = $request->gerai;
            if ($data->update()) {
                flash()->overlay('Data berhasil diEdit.', 'INFO');
                $datas = Akumulasi::where('no_kssd', 'LIKE', '%'.$nomor.'%')->where('aktif', 1)->where('tgl_trx', 'LIKE', '%'.$tahun.'%')->orderBy('id', 'ASC')->get();
                return view('administrator.saldo_akumulasi', compact('dashboard', 'users', 'nomor', 'datas', 'tahun'));
            }
            flash()->overlay('Data gagal diEdit.', 'INFO');
            $datas = Akumulasi::where('no_kssd', 'LIKE', '%'.$nomor.'%')->where('aktif', 1)->where('tgl_trx', 'LIKE', '%'.$tahun.'%')->orderBy('id', 'ASC')->get();
            return view('administrator.saldo_akumulasi', compact('dashboard', 'users', 'nomor', 'datas', 'tahun'));
        } elseif ($request->action =='hapus') {
            $tahun = $request->tahun;
            if (!$users = User::where('aktif', 1)->where('no_anggota', 'LIKE', '%'.$nomor.'%')->where('type', '<>', 'karyawan')->first()) {
                flash()->overlay('Nomor tidak Terdaftar.', 'INFO');
                return redirect()->back();
            }
            $data = Akumulasi::find($request->ids);
            $data->aktif = 0;
            if ($data->update()) {
                flash()->overlay('Data berhasil diHapus.', 'INFO');
                $datas = Akumulasi::where('no_kssd', 'LIKE', '%'.$nomor.'%')->where('aktif', 1)->where('tgl_trx', 'LIKE', '%'.$tahun.'%')->orderBy('id', 'ASC')->get();
                return view('administrator.saldo_akumulasi', compact('dashboard', 'users', 'nomor', 'datas', 'tahun'));
            }
            flash()->overlay('Data gagal diHapus.', 'INFO');
            $datas = Akumulasi::where('no_kssd', 'LIKE', '%'.$nomor.'%')->where('aktif', 1)->where('tgl_trx', 'LIKE', '%'.$tahun.'%')->orderBy('id', 'ASC')->get();
            return view('administrator.saldo_akumulasi', compact('dashboard', 'users', 'nomor', 'datas', 'tahun'));
        }
        return view('administrator.saldo_akumulasi', compact('dashboard', 'users', 'nomor', 'datas', 'tahun'));
    }
    public function alert(Request $request)
    {
        $dashboard ="Simpananwajib";
        $alertbulan = date('Y');
        $thn = date('Y');
        $bln = date('m');
        $no_anggota = $request->no_anggota;
        $stts = 'Semua';
        $cari ='tidak';
        if ($request->action =='cari') {
            // return $request->all();
            $thn = $request->tahun;
            $no_anggota = $request->no_anggota;
            $alertbulan = $request->tahun;
            $bln = $request->bulan;
            $stts = $request->status;
            $cari ='cari';
        } elseif ($request->action =='export') {
            $thn = $request->tahun;
            $no_anggota = $request->no_anggota;
            $alertbulan = $request->tahun;
            $bln = $request->bulan;
            $stts = $request->status;
            $cari ='cari';
        }
        $wajib = JenisSimpanan::find(2);
        $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$no_anggota.'%')->where('jenis_simpanan', 2)->where('mutasi', 'Kredit')->where('aktif', 1)->where('awal', 1)->orderBy('jth_tempo', 'ASC')->get();
        if ($request->action =='export' && $request->excel ==1) {
            return Excel::create($bln.'-'.$alertbulan, function ($excel) use ($thn,$bln,$stts,$no_anggota,$alertbulan,$simpanan) {
                $excel->sheet('Excel sheet', function ($sheet) use ($thn,$bln,$stts,$no_anggota,$alertbulan,$simpanan) {
                    $sheet->loadView('administrator.report._report_status_simpanan_wajib', compact('alertbulan', 'bln', 'thn', 'no_anggota', 'stts', 'wajib', 'cari', 'simpanan'));
                });
            })->export('xls');
        }
        return view('administrator.alert_simpanan_wajib', compact('dashboard', 'simpanan', 'alertbulan', 'bln', 'thn', 'no_anggota', 'stts', 'wajib', 'cari'));
    }

    public function jthtempo(Request $request)
    {
        $dashboard ="Simpananwajib";
        $alertbulan = date('Y');
        $thn = date('Y');
        $bln = date('m');
        $no_anggota = $request->no_anggota;
        $stts = 'Semua';
        $cari ='tidak';
        if ($request->action =='cari') {
            $thn = $request->tahun;
            $no_anggota = $request->no_anggota;
            $alertbulan = $request->tahun;
            $bln = $request->bulan;
            $stts = $request->status;
            $cari ='cari';
        } elseif ($request->action =='export') {
            $thn = $request->tahun;
            $no_anggota = $request->no_anggota;
            $alertbulan = $request->tahun;
            $bln = $request->bulan;
            $stts = $request->status;
            $cari ='cari';
        }
        $wajib = JenisSimpanan::find(2);
        $user = User::where('aktif', 1)->where('no_anggota', 'LIKE', '%'.$no_anggota.'%')->where('type', '<>', 'karyawan')->orderBy('name', 'ASC')->get();
        // $simpanan = Simpananadmin::where('jenis_simpanan',2)->where('mutasi','Kredit')->where('aktif',1)->where('tgl_setor','LIKE','%'.$thn.'%')->orderBy('name','ASC')->get();
        if ($request->action =='export' && $request->excel ==1) {
            return Excel::create($bln.'-'.$alertbulan, function ($excel) use ($user,$thn,$bln,$stts,$no_anggota,$alertbulan) {
                $excel->sheet('Excel sheet', function ($sheet) use ($user,$thn,$bln,$stts,$no_anggota,$alertbulan) {
                    $sheet->loadView('administrator.report._report_data_simpanan_wajib', compact('user', 'alertbulan', 'bln', 'thn', 'no_anggota', 'stts', 'wajib', 'cari'));
                });
            })->export('xls');
        }
        return view('administrator.simpanan_jatuh_tempo', compact('dashboard', 'user', 'alertbulan', 'bln', 'thn', 'no_anggota', 'stts', 'wajib', 'cari'));
    }
    public function resetpassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6'
        ]);
          $edit = User::find($request->ids)->update([
            'password'=>Hash::make($request->password),
            'admin'=>$request->user()->id
          ]);
          Log::info('RSET PASSWORD :'.$request->password);
          if ($edit) {
              flash()->overlay('Password berhasil diganti.', 'INFO');
              return redirect()->back();
          }
        flash()->overlay('Password gagal diganti.', 'GAGAL');
        return redirect()->back();
    }
}
