<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Komunitas;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use App\User;
use App\Simpanan;
use App\Berita;
use App\Simpananadmin;
use App\Akumulasi;
use App\City;
use App\Pinjaman;
use Image;
use Excel;
use DB;
use Log;

class LaporanController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }
  public function simpananadmin(Request $request)
  {
      $dashboard ="laporan";
      $nomor = $request->no_anggota;
      $jenissim = $request->jenis_simpanan;
      $from = $request->dari;
      $to = $request->sampai;
      $mutasis = $request->mutasi;
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
      if ($request->action =='cari') {
          $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 'LIKE', '%'.$jenissim.'%')->where('mutasi', 'Kredit')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
          return view('administrator.laporan.simpanan', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
      } elseif ($request->action =='csv') {
          /////////////////////////
          $validatedData = $request->validate([
      'upload' => 'required'
      ]);
          $ktp = '12345.'.$request->upload->getClientOriginalExtension();
          $sequence = substr($ktp, -3);
          if ($sequence !=='csv') {
              flash()->overlay('Gagal, Data Upload harus CSV File.', 'INFO');
              return redirect()->back();
          }
          $upload = $request->file('upload');
          $filePath = $upload->getRealPath();
          $file = fopen($filePath, 'r');
          $header = fgetcsv($file);
          $escapedHeader=[];
          //validate
          foreach ($header as $key => $value) {
              $lheader=strtolower($value);
              $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
              array_push($escapedHeader, $escapedItem);
          }
          //looping through othe columns
          while ($columns=fgetcsv($file)) {
              if ($columns[0]=="") {
                  continue;
              }
              //trim data
              foreach ($columns as $key => &$value) {
                  $value=$value;
              }
              // return $columns;
              $data = array_combine($escapedHeader, $columns);
              // setting type
              foreach ($data as $key => &$value) {
                  $value=($key=="nokssd" || $key=="tanggal" || $key=="kode")?(string)$value: (string)$value;
              }
              // return $data;
              // Table update
              $nokssds=$data['nokssd'];
              $namas=$data['nama'];
              $tanggals=$data['tanggal'];
              $notrx=$data['nomortransaksi'];
              $nominals = $data['nominal'];
              $saldos = $data['saldo'];
              $kodes = $data['kode'];
              $simpanan = Simpananadmin::firstOrNew(['no_anggota'=>$nokssds,'tgl_setor'=>$tanggals,'saldo'=>$saldos,'no_trx'=>$notrx]);
              $simpanan->no_anggota=$nokssds;
              $simpanan->tgl_setor = $tanggals;
              $simpanan->no_trx = $notrx;
              $simpanan->mutasi='Debet';
              $simpanan->nominal=$nominals;
              $simpanan->saldo=$saldos;
              $simpanan->ket=$kodes;
              $simpanan->aktif=1;
              $simpanan->jenis_simpanan=$kodes;
              $simpanan->petugas = $request->user()->id;
              $simpanan->save();
          }
          flash()->overlay('CSV Berhasil di Upload.', 'INFO');
          return redirect()->back();
      }
      $from = date('01-m-Y');
      $to = date('d-m-Y');
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d');
      $simpanan = Simpananadmin::where('no_anggota','LIKE','%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('aktif', 1)->where('mutasi', 'Kredit')->orderBy('tgl_setor', 'ASC')->get();
      return view('administrator.laporan.simpanan', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
  }
  public function pinjaman(Request $request){
    $dashboard ="laporan";
    $nomor = $request->no_anggota;
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d');
    if ($request->action =="cari") {
      $from = date('d-m-Y', strtotime($request->dari));
      $to = date('d-m-Y', strtotime($request->sampai));
      $dari = date('Y-m-d', strtotime($from));
      $ke = date('Y-m-d', strtotime($to));
    }
    $pinjamans = Pinjaman::where('no_anggota','LIKE','%'.$nomor.'%')->whereBetWeen('tgl_trx', [$dari,$ke])->where('status_pinjam','LIKE','%'.$request->status_pinjam.'%')->where('aktif', 1)->orderBy('tgl_trx', 'ASC')->get();
    return view('administrator.laporan.pinjaman', compact('dashboard', 'nomor', 'pinjamans', 'from', 'to'));
  }
}
