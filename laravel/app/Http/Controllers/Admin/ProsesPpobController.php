<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Datatransaksippobs;
use App\Bukusaldotransaksi;
use App\Product;
use App\User;
use App\Website;
use Auth;
use DB;
use Log;

class ProsesPpobController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function prosesppob(Request $request)
    {
        $params['code'] = '400';
        $params['messages'] = "<p class='alert alert-danger'>Admin tidak boleh transaksi</p>";
        return $params;

        if ($request->nomorhp =="" || $request->paket =="") {
            $params['code'] = '400';
            $params['messages'] = "<p class='alert alert-danger'>Isi form dengan lengkap</p>";
            return $params;
        }
        $this->validate($request, [
          'nomorhp' => 'required',
          'paket' => 'required'
      ]);
        $harga = Product::where('code', $request->paket)->first();
        if (Auth::user()->saldotransaksi > $harga->jual) {
            if (date('H') > 23 || date('H') < 1) {
                $params['code'] = '400';
                $params['messages'] = "<p class='alert alert-danger'>Saat ini sedang maintenance jam 23:00 s/d 01:00 WIB</p>";
                return $params;
            }
            $saldo = Auth::user()->saldotransaksi - $harga->jual;
            $trxid = date('YmdHis').$request->user()->id;
            DB::beginTransaction();
            try {
                Datatransaksippobs::create([
              'user_id'=>$request->user()->id,
              'no_anggota'=>$request->user()->no_anggota,
              'no_trx'=>$trxid,
              'tgl_trx'=>date('Y-m-d'),
              'mutasi'=>'Kredit',
              'paket'=>$harga->description,
              'nominal'=>$harga->jual,
              'nta'=>$harga->price,
              'saldo'=>$saldo,
              'status'=>'Proses',
              'nomorhp'=>$request->nomorhp,
              'aktif'=>1,
              'keterangan'=>'Transaksi '.$harga->description,
              'created_by'=>$request->user()->id
            ]);
                User::find($request->user()->id)->update([
              'saldotransaksi'=>$saldo
            ]);
            } catch (\Exception $e) {
                Log::info('Gagal Transaksi PPOB:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Transaksi GAGAL.', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            // ????????
            $params['trxid']=$trxid;
            $params['nomor']=$request->nomorhp;
            $params['code']=$request->paket;
            $access_token = Website::find(1);
            $url = $access_token->url.'proses-agent';
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', $url, [
                              'headers'=>[
                                  'Accept' => 'application/json',
                                  'Content-Type' => 'application/json',
                                  'Authorization' => 'Bearer '.$access_token->token
                              ],
                                    'body'=>json_encode($params)
                          ]);
            if ($res->getStatusCode() == 200) {
                $hasil =  json_decode($res->getBody()->getContents(), true);
                if ($hasil['result'] =='success') {
                    Bukusaldotransaksi::create([
                      'user_id'=>$request->user()->id,
                      'no_anggota'=>$request->user()->no_anggota,
                      'no_trx'=>$trxid,
                      'tgl_trx'=>date('Y-m-d'),
                      'nominal'=>$harga->jual,
                      'saldo'=>$saldo,
                      'mutasi'=>'Debet',
                      'keterangan'=>'Transaksi '.$harga->description.' '.$request->nomorhp,
                      'aktif'=>1,
                      'created_by'=>$request->user()->id,
                    ]);
                    $url = route('data-transaksi-ppob');
                    $nominal = number_format($harga->jual, 0, ",", ".");
                    $params['code'] = '200';
                    $pesan = $hasil['message'];
                    $params['messages'] = "<table class='table' id='table'>
                      <tr>
                        <td>Produk</td><td>: $harga->description </td>
                      </tr>
                      <tr>
                        <td>Nominal</td><td>: Rp $nominal </td>
                      </tr>
                      <tr>
                        <td>Nomor Pelanggan</td><td>: $request->nomorhp </td>
                      </tr>
                      <tr>
                        <td>Status</td><td>: Proses </td>
                      </tr>
                      <tr>
                        <td colspan='2'> $pesan </td>
                      </tr>
                      <tr>
                        <td align='center' colspan='2'> <a href='$url'><button class='btn btn-primary'>Data Transaksi</button></a> </td>
                      </tr>
                    </table>";
                    return $params;
                // flash()->overlay($harga->description.' '.$request->nomorhp.' Mohon di tunggu', 'Berhasil');
                    // return redirect()->route('data-transaksi-ppob');
                } else {
                    $hapus = Datatransaksippobs::where('no_trx', $trxid)->first();
                    $hapus->delete();
                    $users = User::find($request->user()->id);
                    $users->saldotransaksi = $users->saldotransaksi + $harga->jual;
                    $users->update();
                    $params['code'] = '400';
                    $params['messages'] = "<p class='alert alert-danger'>Transaksi Gagal, Silahkan di ulangi kembali</p>";
                    return $params;
                }
            } else {
                $params['code'] = '400';
                $params['messages'] = "<p class='alert alert-danger'>Gagalmenampilkan data</p>";
                return $params;
            }
        }
        $params['code'] = '400';
        $params['messages'] = "<p class='alert alert-danger'>SALDO anda Tidak Cukup</p>";
        return $params;
    }
}
