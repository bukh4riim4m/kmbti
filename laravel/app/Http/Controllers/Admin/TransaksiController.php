<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Komunitas;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use App\User;
use App\Simpanan;
use App\Berita;
use App\Simpananadmin;
use App\Akumulasi;
use App\Pinjaman;
use App\BayarPinjaman;
use Excel;
use DB;
use Log;
// use Charts;
class TransaksiController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }
  public function adminsimpananpokok(Request $request){
    
    $dashboard ="transaksi";
    $nomor = $request->no_anggota;
    $jenissim = $request->jenis_simpanan;
    $from = $request->dari;
    $to = $request->sampai;
    $mutasis = $request->mutasi;
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    if ($request->action =='cari') {
        $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan',1)->where('mutasi', 'Kredit')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
        return view('administrator.transaksi.simpanan_pokok', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
    } elseif ($request->action =='csv') {
        /////////////////////////
        $validatedData = $request->validate([
    'upload' => 'required'
    ]);
        $ktp = '12345.'.$request->upload->getClientOriginalExtension();
        $sequence = substr($ktp, -3);
        if ($sequence !=='csv') {
            flash()->overlay('Gagal, Data Upload harus CSV File.', 'INFO');
            return redirect()->back();
        }
        $upload = $request->file('upload');
        $filePath = $upload->getRealPath();
        $file = fopen($filePath, 'r');
        $header = fgetcsv($file);
        $escapedHeader=[];
        //validate
        foreach ($header as $key => $value) {
            $lheader=strtolower($value);
            $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
            array_push($escapedHeader, $escapedItem);
        }
        //looping through othe columns
        while ($columns=fgetcsv($file)) {
            if ($columns[0]=="") {
                continue;
            }
            //trim data
            foreach ($columns as $key => &$value) {
                $value=$value;
            }
            // return $columns;
            $data = array_combine($escapedHeader, $columns);
            // setting type
            foreach ($data as $key => &$value) {
                $value=($key=="nokssd" || $key=="tanggal" || $key=="kode")?(string)$value: (string)$value;
            }
            // return $data;
            // Table update
            $nokssds=$data['nokssd'];
            $namas=$data['nama'];
            $tanggals=$data['tanggal'];
            $notrx=$data['nomortransaksi'];
            $nominals = $data['nominal'];
            $saldos = $data['saldo'];
            $kodes = $data['kode'];
            $simpanan = Simpananadmin::firstOrNew(['no_anggota'=>$nokssds,'tgl_setor'=>$tanggals,'saldo'=>$saldos,'no_trx'=>$notrx]);
            $simpanan->no_anggota=$nokssds;
            $simpanan->tgl_setor = $tanggals;
            $simpanan->no_trx = $notrx;
            $simpanan->mutasi='Debet';
            $simpanan->nominal=$nominals;
            $simpanan->saldo=$saldos;
            $simpanan->ket=$kodes;
            $simpanan->aktif=1;
            $simpanan->jenis_simpanan=$kodes;
            $simpanan->petugas = $request->user()->id;
            $simpanan->save();
        }
        flash()->overlay('CSV Berhasil di Upload.', 'INFO');
        return redirect()->back();
    }
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d');
    $simpanan = Simpananadmin::whereBetWeen('tgl_setor', [$dari,$ke])->where('aktif', 1)->where('mutasi', 'LIKE', '%'.$mutasis.'%')->orderBy('tgl_setor', 'ASC')->get();
    return view('administrator.transaksi.simpanan_pokok', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
  }
  public function adminsimpananwajib(Request $request){
    $dashboard ="transaksi";
    $nomor = $request->no_anggota;
    $jenissim = $request->jenis_simpanan;
    $from = $request->dari;
    $to = $request->sampai;
    $mutasis = $request->mutasi;
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d', strtotime($to));
    if ($request->action =='cari') {
        $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan',2)->where('mutasi', 'Kredit')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
        return view('administrator.transaksi.simpanan_wajib', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
    } elseif ($request->action =='csv') {
        /////////////////////////
        $validatedData = $request->validate([
    'upload' => 'required'
    ]);
        $ktp = '12345.'.$request->upload->getClientOriginalExtension();
        $sequence = substr($ktp, -3);
        if ($sequence !=='csv') {
            flash()->overlay('Gagal, Data Upload harus CSV File.', 'INFO');
            return redirect()->back();
        }
        $upload = $request->file('upload');
        $filePath = $upload->getRealPath();
        $file = fopen($filePath, 'r');
        $header = fgetcsv($file);
        $escapedHeader=[];
        //validate
        foreach ($header as $key => $value) {
            $lheader=strtolower($value);
            $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
            array_push($escapedHeader, $escapedItem);
        }
        //looping through othe columns
        while ($columns=fgetcsv($file)) {
            if ($columns[0]=="") {
                continue;
            }
            //trim data
            foreach ($columns as $key => &$value) {
                $value=$value;
            }
            // return $columns;
            $data = array_combine($escapedHeader, $columns);
            // setting type
            foreach ($data as $key => &$value) {
                $value=($key=="nokssd" || $key=="tanggal" || $key=="kode")?(string)$value: (string)$value;
            }
            // return $data;
            // Table update
            $nokssds=$data['nokssd'];
            $namas=$data['nama'];
            $tanggals=$data['tanggal'];
            $notrx=$data['nomortransaksi'];
            $nominals = $data['nominal'];
            $saldos = $data['saldo'];
            $kodes = $data['kode'];
            $simpanan = Simpananadmin::firstOrNew(['no_anggota'=>$nokssds,'tgl_setor'=>$tanggals,'saldo'=>$saldos,'no_trx'=>$notrx]);
            $simpanan->no_anggota=$nokssds;
            $simpanan->tgl_setor = $tanggals;
            $simpanan->no_trx = $notrx;
            $simpanan->mutasi='Debet';
            $simpanan->nominal=$nominals;
            $simpanan->saldo=$saldos;
            $simpanan->ket=$kodes;
            $simpanan->aktif=1;
            $simpanan->jenis_simpanan=$kodes;
            $simpanan->petugas = $request->user()->id;
            $simpanan->save();
        }
        flash()->overlay('CSV Berhasil di Upload.', 'INFO');
        return redirect()->back();
    }
    $from = date('01-m-Y');
    $to = date('d-m-Y');
    $dari = date('Y-m-d', strtotime($from));
    $ke = date('Y-m-d');
    $simpanan = Simpananadmin::whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan',2)->where('aktif', 1)->where('mutasi', 'LIKE', '%'.$mutasis.'%')->orderBy('tgl_setor', 'ASC')->get();
    return view('administrator.transaksi.simpanan_wajib', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
  }
  public function adminsimpanansukarela(Request $request){
    $dashboard ="transaksi";
    return view('administrator.transaksi.simpanan_sukarela', compact('dashboard'));

  }
  public function adminsimpanankhusus(Request $request){
    $dashboard ="transaksi";
    return view('administrator.transaksi.simpanan_khusus', compact('dashboard'));

  }
  // public function historysimpanan(Request $request){
  //   return 'OK';
  // }
  public function cekanggota(Request $request){
    if ($request->ajax()) {
      if ($anggota = User::where('no_anggota',$request->noanggota)->where('aktif',1)->where('type','anggota')->first()) {
        $hasil = "<div class='col-sm-6'>
          <div class='form-group'>
            <label class='control-label'>Nama Anggota<span class='text-danger'>*</span></label>
            <label class='form-control btn-success'>".$anggota->name."</label>
          </div>
        </div>
        <div class='col-sm-6'>
          <div class='form-group'>
            <label class='control-label'>Nomor Hp<span class='text-danger'>*</span></label>
            <label class='form-control btn-success'>".$anggota->telp."</label>
          </div>
        </div>";
      }else {
        $errors = "<div class='col-sm-12'>
                    <div class='form-group'>

                      <label class='form-control btn btn-danger'>NOMOR ANGGOTA TIDAK DI TEMUKAN</label>
                    </div>
                  </div>";
      }
      if ($anggota) {
        $response = [
          'code'=>200,
          'datas'=>$hasil
        ];
      }else {
        $response = [
          'code'=>400,
          'datas'=>$errors
        ];
      }

      return $response;
    }
  }
  public function pinjaman(Request $request){
    $dashboard ="transaksi";
    if ($request->action == "proses") {
      if (!$user = User::where('no_anggota',$request->no_anggota)->where('type','anggota')->where('aktif',1)->first()) {
        flash()->overlay('Nomor anggota salah.', 'INFO');
        return redirect()->back();
      }
      $this->validate($request, [
            'tgl_tempo' => 'required',
            'no_pinjam' =>'required',
            'no_anggota' =>'required',
            'nilai_pinjam' =>'required',
            'persen_bunga' =>'required',
            'ket' =>'required'
      ]);
      $jatuhtempo = date('Y-m-d', strtotime($request->tgl_tempo));
      //Cari jumlah bulan
      $tglsetor = date_create(date('Y-m-d'));
      $tgltempo = date_create(date('Y-m-d', strtotime($request->tgl_tempo)));
      $diff = date_diff($tglsetor,$tgltempo);
      $bulan = $diff->y*12 + $diff->m + $diff->d/30;
      $nilaibulan = round($bulan);

      $nominals= str_replace(".", "", $request->nilai_pinjam);
      $nilaibunga = (int)$nominals * (int)$request->persen_bunga / 100;
      $nilaiangsuran = ($nominals + $nilaibunga) / $nilaibulan;
      $nilaitotal = (int)$nominals + $nilaibunga;

      DB::beginTransaction();
      try {
        $proses = Pinjaman::create([
          'no_pinjam' => $request->no_pinjam,
          'no_anggota' =>$user->no_anggota,
          'tgl_trx' =>date('Y-m-d'),
          'jatuh_tempo' =>$jatuhtempo,
          'kali_angsuran'=>$nilaibulan,
          'nilai_pinjam' =>$nominals,
          'bunga_persen' =>$request->persen_bunga,
          'nilai_bunga' =>$nilaibunga,
          'angsuran' =>$nilaiangsuran,
          'total_pinjam' =>$nilaitotal,
          'aktif' =>1,
          'admin' =>$request->user()->id,
          'status_pinjam' =>'Belum Lunas',
          'sisa_pinjam' =>$nilaitotal,
          'keterangan'=>$request->ket
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal Proses Pinjaman:'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Gagal Edit Profil.','INFO');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Pinjaman berhasil di proses.', 'INFO');
      return redirect()->back();
    }
    return view('administrator.transaksi.form_pinjaman', compact('dashboard'));
  }
  public function bayarpinjaman(Request $request){
    $dashboard ="transaksi";
    return view('administrator.transaksi.bayar_pinjaman', compact('dashboard'));
  }
  public function kalkulasi(Request $request){
    if ($request->ajax()) {
      Log::info('REQUES:'.$request->nilaipinjam.','.$request->persenbunga.','.$request->tglsetor.','.$request->tgltempo);

      $tglsetor = date_create(date('Y-m-d', strtotime($request->tglsetor)));
      $tgltempo = date_create(date('Y-m-d', strtotime($request->tgltempo)));
      $diff = date_diff($tglsetor,$tgltempo);
      $bulan = $diff->y*12 + $diff->m + $diff->d/30;
      $nilaibulan = round($bulan);

      $nominals= str_replace(".", "", $request->nilaipinjam);
      $nilaibunga = (int)$nominals * (int)$request->persenbunga / 100;
      $nilaiangsuran = ($nominals + $nilaibunga) / $nilaibulan;
      $nilaitotal = (int)$nominals+$nilaibunga;
    }
    $nilaibulan2 = "<input class='form-control' type='text' name='nilaibunga' value='".number_format($nilaibulan,0,',','.')." Kali'  disabled>";
    $nilaibunga2 = "<input class='form-control' type='text' name='angsuran' value='".number_format($nilaibunga,0,',','.')."'  disabled>";
    $nilaiangsuran2 = "<input class='form-control' type='text' name='nominal_angsuran' value='".number_format($nilaiangsuran,0,',','.')."'  disabled>";
    $nilaitotal2 = "<input class='form-control' type='text' name='jumlah_total' value='".number_format($nilaitotal,0,',','.')."'  disabled>";

    $datas = [
      'nilaibulan'=>$nilaibulan2,
      'nilaibunga'=>$nilaibunga2,
      'nilaiangsuran'=>$nilaiangsuran2,
      'nilaitotal'=>$nilaitotal2
    ];
    if ($datas) {
      $respon = [
        'code'=>200,
        'isi'=>$datas
      ];
    }

    return $respon;
  }
}
