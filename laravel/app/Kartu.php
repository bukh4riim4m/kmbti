<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kartu extends Model
{
  protected $fillable = [
      'id','no_anggota','foto','created_at','updated_at'
  ];
}
