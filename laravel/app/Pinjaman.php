<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
  protected $fillable = [
    'id','no_pinjam','no_anggota','tgl_trx','jatuh_tempo','kali_angsuran','nilai_pinjam','bunga_persen','nilai_bunga','angsuran','total_pinjam','keterangan','aktif','admin','status_pinjam','sisa_pinjam','created_at','updated_at','deleted_at','deleted_with'
  ];
}
