<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaldoKasbank extends Model
{
  protected $fillable = [
      'id','name','aktif','created_at','updated_at'
  ];
}
