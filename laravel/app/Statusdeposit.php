<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statusdeposit extends Model
{
  protected $fillable = [
      'id','status','aktif','created_at','updated_at'
  ];
}
