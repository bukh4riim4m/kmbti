@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">


<div class="row">
	<div class="col-xs-12">
		<h4 class="page-title">Data Transaksi PPOB</h4>
	</div>
  <!-- <div class="col-xs-6">
    <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i> Tambah Kecamatan</a>
  </div> -->
</div>

<div class="row filter-row">
  <form class="" action="{{url('/administrator/data-transaksi-ppob')}}" method="post">
    @csrf
<input type="hidden" name="action" value="cari">
	<div class="col-sm-6 col-xs-6">
		<div class="form-group form-focus">
			<label class="control-label">Nomor Transaksi/Pelanggan</label>
			<input type="text" name="nomor" value="" class="form-control floating">
		</div>
	</div>
	<div class="col-sm-6 col-xs-6">
		<!-- <a href="#" class="btn btn-success btn-block"> Search </a> -->
    <input type="submit" class="btn btn-success btn-block" name="btn" value="Search">
	</div>
</form>
</div>

<div class="row">
	<div class="col-sx-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped custom-table datatable">
			<!-- <a href="{{url('#')}}" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i>Tambah Golongan</a> -->

				<thead>
					<tr>
						<th>No.</th>
						<th>No Transaksi</th>
						<th>Tgl Transaksi</th>
            <th>Nomor Pelanggan</th>
            <th>Paket</th>
            <th>Harga</th>
            <th>Status</th>
            <th style="max-width:70px;">Action</th>
					</tr>
				</thead>
				<tbody>
          <?php $no=1; ?>
@foreach($datas as $data)
					<tr class="holiday-completed">
						<td>{{$no++}}</td>
						<td>{{$data->no_trx}}</td>
            <td>{{$data->tgl_trx}}</td>
            <td>{{$data->nomorhp}}</td>
            <td>{{$data->paket}}</td>
            <td>{{number_format($data->nominal,0,",",".")}}</td>
            <td>{{$data->status}}</td>
						<td style="max-width:70px;">
							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_komunitas">Refresh</a>
						</td>
					</tr>
@endforeach
				</tbody>

			</table>
		</div>
	</div>
</div>

</div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
</div>
</div>
@endsection
