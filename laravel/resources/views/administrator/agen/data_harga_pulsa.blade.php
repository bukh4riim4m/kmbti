@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Daftar Harga</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30">
  <div class="view-icons">
  </div>
</div> -->
</div>
<div class="row filter-row">
  <form class="form" action="{{url('/administrator/harga-ppob')}}" method="post">
    @csrf
    <input type="hidden" name="action" value="cari">
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Nama Provider</label>
        <input type="text" class="form-control floating" name="provider" value="{{$providers}}"/>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Deskripsi</label>
        <input type="text" class="form-control floating" name="description" value="{{$deskropsi}}"/>
      </div>
    </div>
    <div class="col-sm-3 col-md-3 col-xs-12">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Status</label>
        <?php $statuses = ['Normal','Gangguan']; ?>
        <select class="select floating" name="status">
          <option value="">Semua</option>
          @foreach($statuses as $status)
            @if($stts == $status)
              <option value="{{$status}}" selected> {{$status}} </option>
            @else
              <option value="{{$status}}"> {{$status}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>


</div>
<div class="row">
<div class="col-md-12">
  <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
              var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
              angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
              }
              function rupiah(){ var nominal = document.getElementById("nominal").value; var rupiah = convertToRupiah(nominal);
              document.getElementById("nominal").value = rupiah; }
              </script>
              <script> function convertToRupiah (objek) {
               separator = ".";
               a = objek.value;
               b = a.replace(/[^\d]/g,"");
               c = "";
               panjang = b.length;
               j = 0; for (i = panjang; i > 0; i--) {
               j = j + 1; if (((j % 3) == 1) && (j != 1)) {
               c = b.substr(i-1,1) + separator + c; } else {
               c = b.substr(i-1,1) + c; } } objek.value = c; }
               </script>
  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>Provider</th>
          <th>Deskripsi</th>
          <th>Harga Jual</th>
          <th>Harga dari Suplayer</th>
          <th>Keuntungan Koperasi</th>
          <th>status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;?>
        @foreach($pulsa as $pul)
        <?php $untung = $pul['jual']-$pul['price']; ?>
        @if($untung < 200 || $pul['status']=='gangguan')
        <tr style="background:red;">
        @else
        <tr>
        @endif
          <td>{{$no++}}.</td>
          <td>{{$pul['operator']}}</td>
          <td>{{$pul['description']}}</td>
          <td>Rp. {{number_format($pul['jual'],0,",",".")}}</td>
          <td>Rp. {{number_format($pul['price'],0,",",".")}}</td>
          <td>Rp. {{number_format($pul['jual']-$pul['price'],0,",",".")}}</td>
          <td>{{$pul['status']}}</td>
          <td style="max-width:60px;" class="text-right">
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit{{$pul->id}}">Edit</a>
          </td>
        </tr>
        <div id="edit{{$pul->id}}" class="modal custom-modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content modal-md">
              <div class="modal-header">
                <h4 class="modal-title">Edit Harga Jual</h4>
              </div>
              <form action="{{url('/administrator/harga-ppob')}}" method="post" id="edit">
                <input type="hidden" name="action" value="edit">
                <input type="hidden" name="ids" value="{{$pul->id}}">
                @csrf
                <input type="text" name="jual" class="form-control" value="{{$pul['jual']}}" onkeyup="convertToRupiah(this)" id="nominal">
                <div class="modal-body card-box">
                  <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Kembali</a>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        @endforeach
        @if(count($pulsa) < 1)
        <tr>
          <td colspan="7" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>


</div>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
