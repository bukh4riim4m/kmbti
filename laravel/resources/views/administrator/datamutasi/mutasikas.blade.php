@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Mutasi Uang Kas</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30"> -->
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Simpanan</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>
</div> -->
</div>
<div class="row filter-row">
  <form class="form" action="{{route('admin-mutasi-kas')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf

    <div class="col-sm-3 col-md-4 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Dari Tahun</label>
        <?php $tahuns = App\Tahun::orderBy('name','ASC')->get(); ?>
        <select class="select floating" name="dari">
          @foreach($tahuns as $thn)
            @if($from == $thn->name)
              <option value="{{$thn->name}}" selected> {{$thn->name}} </option>
            @else
              <option value="{{$thn->name}}"> {{$thn->name}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-md-4 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Sampai Tahun</label>
        <?php $tahuns = App\Tahun::orderBy('name','ASC')->get(); ?>
        <select class="select floating" name="sampai">
          @foreach($tahuns as $thn)
            @if($until == $thn->name)
              <option value="{{$thn->name}}" selected> {{$thn->name}} </option>
            @else
              <option value="{{$thn->name}}"> {{$thn->name}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div>

<div class="row">

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>Tgl.Trx</th>
          <th>No.Angota</th>
          <th>Debet</th>
          <th>Kredit</th>
          <th>Saldo</th>
          <th>Keterangan</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;
        $masuk=0;$keluar=0;$saldos=0;$saldosawal=0;$saldosakhir=0;$maxs=array();$maks=array();
        ?>
@foreach($mutasikases as $key => $mutasikas)
        <?php if ($mutasikas->mutasi =='Kredit') {
            $masuk+=$mutasikas->nominal;
            if ($key ==0) {
                $saldosawal = $mutasikas->saldo - $mutasikas->nominal;
            }
        } else {
            $keluar+=$mutasikas->nominal;
            if ($key ==0) {
                $saldosawal = $mutasikas->saldo + $mutasikas->nominal;
            }
        };
        $saldosakhir = $mutasikas->saldo;?>
        <tr>
          <td>{{$no++}}.</td>
          <td>{{$mutasikas->tgl_setor}}</td>
          <td>{{$mutasikas->no_anggota}}</td>
          @if($mutasikas->mutasi =='Debet')
          <td>Rp {{number_format($mutasikas->nominal,0,",",".")}}</td>
          <td>-</td>
          @else
          <td>-</td>
          <td>Rp {{number_format($mutasikas->nominal,0,",",".")}}</td>
          @endif
          <td>Rp {{number_format($mutasikas->saldo,0,",",".")}}</td>
          <td>{{$mutasikas->ket}}</td>
        </tr>
@endforeach
{{--@if(count($mutasikases < 0 ))
<tr>
  <td colspan="7" align="center">Kosong</td>
</tr>
@endif--}}
      </tbody>
    </table>
  </div>
</div>

<div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Saldo awal</td>
    <td>: Rp {{number_format($saldosawal,0,",",".")}}</td>
  </tr>
  <tr>
    <td>Total Kredit</td>
    <td>: Rp </td>
  </tr>
  <tr>
    <td>Total Debet</td>
    <td>: Rp </td>
  </tr>
  <tr>
    <td>Saldo Akhir</td>
    <td>: Rp {{number_format($saldosakhir,0,",",".")}}</td>
  </tr>
</table>

</div>
<!-- <form class="" action="{{url('/administrator/data-simpanan')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-2"><br><br>
      <input class="form-control rounded btn-success" type="submit" value="Upload">
  </div>
</form> -->
</div>

    </div>



  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
