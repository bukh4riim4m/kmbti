@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Konfirmasi Deposit</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30">
  <div class="view-icons">
  </div>
</div> -->
</div>
<div class="row filter-row">
  <form class="form" action="{{route('konfirmasi-deposit')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf

    <div class="col-sm-4 col-md-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value=""></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value=""></div>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{('#')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="nip" value=""/>
    <input type="hidden" name="hakakses" value=""/>
    <input type="hidden" name="jabatan" value=""/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table datatable">
      <thead>
        <tr>
          <th>No.</th>
          <th>Nomor Anggota</th>
          <th>Tgl. Transaksi</th>
          <th>No.Transaksi</th>
          <th>Bank</th>
          <th>Nominal</th>
          <th>Status</th>
          <th style="max-width:120px;" class="text-right">Action</th>
        </tr>
      </thead>
      <tbody><?php $no=1; ?>
        @foreach($deposits as $deposit)
        <tr>
          <td>{{$no++}}</td>
          <td>{{$deposit->no_anggota}}</td>
          <td>{{date('d-m-Y', strtotime($deposit->tgl_trx))}}</td>
          <td>{{$deposit->no_trx}}</td>
          <td>{{$deposit->bank}}</td>
          <td>{{number_format($deposit->nominal,0,",",".")}}</td>
          <td>@if($deposit->status ==1)<span class="label label-warning-border">{{$deposit->statusId->status}}</span>@elseif($deposit->status ==2)<span class="label label-success-border">{{$deposit->statusId->status}}</span>@else <span class="label label-danger-border">{{$deposit->statusId->status}}</span>@endif</td>
          <td style="max-width:120px;" class="text-right">
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#proses{{$deposit->id}}">Proses</a>
            <a href="#" class="btn btn-danger btn-sm rounded" data-toggle="modal" data-target="#batal{{$deposit->id}}">Batalkan</a>
          </td>
        </tr>
        <div id="proses{{$deposit->id}}" class="modal custom-modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content modal-md">
              <div class="modal-header">
                <h4 class="modal-title">Proses Topup Deposit</h4>
              </div>
              <form action="{{url('/administrator/konfirmasi-deposit')}}" method="post">
                <input type="hidden" name="action" value="proses">
                <input type="hidden" name="ids" value="{{$deposit->id}}">
                @csrf
                <div class="modal-body card-box">
                  <p>Yakin Uangnya sudah masuk ???</p>
                  <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Tidak</a>
                    <button type="submit" class="btn btn-primary">YA</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div id="batal{{$deposit->id}}" class="modal custom-modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content modal-md">
              <div class="modal-header">
                <h4 class="modal-title">Batalkan Topup Deposit</h4>
              </div>
              <form action="{{url('/administrator/konfirmasi-deposit')}}" method="post">
                <input type="hidden" name="action" value="batal">
                <input type="hidden" name="ids" value="{{$deposit->id}}">
                @csrf
                <div class="modal-body card-box">
                  <p>Yakin di batalkan ???</p>
                  <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Tidak</a>
                    <button type="submit" class="btn btn-primary">YA</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
