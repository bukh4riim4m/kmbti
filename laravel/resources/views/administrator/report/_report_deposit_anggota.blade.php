<tbody>
  <tr>
    <td>No</td>
    <td>Tanggal Transaksi</td>
    <td>No transaksi</td>
    <td>Bank</td>
    <td>Nominal</td>
    <td>Status</td>
  </tr>
  <?php $no=1;
  $total=0;?>
  @foreach($deposits as $akumulas)
  <?php $total+= $akumulas->nominal;?>
  <tr>
    <td>{{$no++}}</td>
    <td>{{$akumulas->tgl_trx}}</td>
    <td>{{$akumulas->no_trx}}</td>
    <td>{{$akumulas->bank}}</td>
    <td>{{$akumulas->nominal}}</td>
    <td>{{$akumulas->statusId->status}}</td>
  </tr>
  @endforeach
  <tr>
    <td colspan="4">Total</td>
    <td>{{$total}}</td>
  </tr>
</tbody>
