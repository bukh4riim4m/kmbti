<tbody>
  <tr>
    <td>No</td>
    <td>No anggota</td>
    <td>Tanggal Setor</td>
    <td>Jenis Simpanan</td>
    <td>Mutasi</td>
    <td>Nominal</td>
  </tr>
  <?php $no=1;
  $kredit = 0;
  $debet = 0;?>
  @foreach($simpanan as $us)
  <?php if ($us->mutasi =='Kredit') {
    $kredit+=$us->nominal;
  }else {
    $debet+=$us->nominal;
  } ?>
  <tr>
    <td>{{$no++}}</td>
    <td>{{$us->no_anggota}}</td>
    <td>{{$us->tgl_setor}}</td>
    <td>{{$us->jenisSimpanan->name}}</td>
    <td>{{$us->mutasi}}</td>
    <td>{{$us->nominal}}</td>
  </tr>
  @endforeach
  <tr>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Mutasi Debet</td>
    <td>:</td>
    <td>{{$debet}}</td>
  </tr>
  <tr>
    <td></td>
    <td>Mutasi Kredit</td>
    <td>:</td>
    <td>{{$kredit}}</td>
  </tr>
</tbody>
