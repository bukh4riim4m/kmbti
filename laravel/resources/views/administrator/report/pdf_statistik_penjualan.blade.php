<center> <h2><u>STATISTIK PENJUALAN</u></h2></center><br><br>
<table border="1" width="100%">
        <tr>
          <td colspan="3">Penjualan ke Anggota : {{date('d-M-Y', strtotime($dari))}}  s/d  {{date('d-M-Y', strtotime($sampai))}}</td>
        </tr>
        <tr>
          <td style="background-color:#ffff00;width:5px;">No.</td>
          <td style="background-color:#ffff00;">Gerai</td>
          <td style="background-color:#ffff00;">Nominal</td>
        </tr>
        <tr>
          <td>1.</td>
          <td>Ramanuju</td>
          <td>Rp {{number_format($totalramanuju[0]->totalRamanujuAnggota,0,",",".")}}</td>
        </tr>
        <tr>
          <td>2.</td>
          <td>Grogol</td>
          <td>Rp {{number_format($totalgrogol[0]->totalGrogolAnggota,0,",",".")}}</td>
        </tr>
        <tr>
          <td>3.</td>
          <td>PCI</td>
          <td>Rp {{number_format($totalpci[0]->totalPciAnggota,0,",",".")}}</td>
        </tr>
        <tr>
          <td colspan="2" style="background-color:#ffff00;">Total</td>
          <td style="background-color:#ffff00;">Rp {{number_format($totalramanuju[0]->totalRamanujuAnggota+$totalgrogol[0]->totalGrogolAnggota+$totalpci[0]->totalPciAnggota,0,",",".")}}</td>
        </tr>
    </table>
<br>
    <table border="1" width="100%">
    <tr>
      <td colspan="3" bg-color="yellow">Penjualan ke Non Anggota : {{date('d-M-Y', strtotime($dari))}}  s/d  {{date('d-M-Y', strtotime($sampai))}}</td>
    </tr>
    <tr>
      <td style="background-color:#66ff66;width:5px;">No.</td>
      <td style="background-color:#66ff66;">Gerai</td>
      <td style="background-color:#66ff66;">Nominal</td>
    </tr>
    <tr>
      <td>1.</td>
      <td>Ramanuju</td>
      <td>Rp {{number_format($totalramanujunon[0]->totalRamanujuAnggota,0,",",".")}}</td>
    </tr>
    <tr>
      <td>2.</td>
      <td>Grorol</td>
      <td>Rp {{number_format($totalgrogolnon[0]->totalGrogolAnggota,0,",",".")}}</td>
    </tr>
    <tr>
      <td>3.</td>
      <td>PCI</td>
      <td>Rp {{number_format($totalpcinon[0]->totalPciAnggota,0,",",".")}}</td>
    </tr>
    <tr>
      <td colspan="2" style="background-color:#66ff66;">Total</td>
      <td style="background-color:#66ff66;">Rp {{number_format($totalramanujunon[0]->totalRamanujuAnggota+$totalgrogolnon[0]->totalGrogolAnggota+$totalpcinon[0]->totalPciAnggota,0,",",".")}}</td>
    </tr>
  </table>
  <br>
  <table border="1" width="100%">
    <tr>
      <td colspan="3" bg-color="yellow">Total Penjualan : {{date('d-M-Y', strtotime($dari))}}  s/d  {{date('d-M-Y', strtotime($sampai))}}</td>
    </tr>
    <tr>
      <td style="background-color:#66ffcc;width:5px;">No.</td>
      <td style="background-color:#66ffcc;">Gerai</td>
      <td style="background-color:#66ffcc;">Nominal</td>
    </tr>
    <tr>
      <td>1.</td>
      <td>Ramanuju</td>
      <td>Rp {{number_format($totalramanuju[0]->totalRamanujuAnggota+$totalramanujunon[0]->totalRamanujuAnggota,0,",",".")}}</td>
    </tr>
    <tr>
      <td>2.</td>
      <td>Grorol</td>
      <td>Rp {{number_format($totalgrogol[0]->totalGrogolAnggota+$totalgrogolnon[0]->totalGrogolAnggota,0,",",".")}}</td>
    </tr>
    <tr>
      <td>3.</td>
      <td>PCI</td>
      <td>Rp {{number_format($totalpci[0]->totalPciAnggota+$totalpcinon[0]->totalPciAnggota,0,",",".")}}</td>
    </tr>
    <tr>
      <td colspan="2" style="background-color:#66ffcc;">Total</td>
      <td style="background-color:#66ffcc;">Rp {{number_format($totalramanuju[0]->totalRamanujuAnggota+$totalgrogol[0]->totalGrogolAnggota+$totalpci[0]->totalPciAnggota+$totalramanujunon[0]->totalRamanujuAnggota+$totalgrogolnon[0]->totalGrogolAnggota+$totalpcinon[0]->totalPciAnggota,0,",",".")}}</td>
    </tr>
</table>
