@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-6">
  <h4 class="page-title">Simpanan Pokok</h4>
</div>
<div class="col-xs-6 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Transaksi Simpanan</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>-->
</div>
</div>
<div class="row filter-row">
  <div class="modal-body">
    <form class="m-b-30" action="{{route('admin-simpan-pokok')}}" method="post">
      @csrf
      <input type="hidden" name="action" value="tambah">
      <div class="row">
        <div id="respon"></div>
        <div class="col-sm-12">
          <div class="form-group">
            <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="no_anggota" minlength="6" required id="no_anggota">
          </div>
        </div>
        <script type="text/javascript">
        console.log('masuk javascript');
        $("input[name='no_anggota']").on("change keyup paste", function(){
          var noanggota = $("#no_anggota").val();
          var token = $("input[name='_token']").val();

            if (noanggota.length>=12) {
              $("#transactionLoader").show();
              $.ajax({
                url: "<?php echo route('admin-check-anggota') ?>",
                method:'POST',
                data:{_token:token, noanggota:noanggota},
                success:function(data){
                  console.log(data);
                  if (data.code=200) {
                    $("#respon").html("");
                    $("#respon").append(data.datas);
                  }else if (data.code=400){
                    alert('Nomor Anggota Tidak Terdaftar');
                  }
                }
              });
              $("#transactionLoader").hide();
            }

          });
        </script>

        <div id="transactionLoader">
          <center> <div id="wait"><div class='col-md-12'><div class='loading9 text-center'><i></i><i></i><i></i><i></i></div></div></div></center>
        </div>
        <script>
          $("#transactionLoader").hide();
        </script>
        <div>
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Tanggal Setor <span class="text-danger">*</span></label>
              <div class="cal-icon"><input class="form-control datetimepicker" type="text" value="{{date('dd-mm-Y')}}" name="tgl_setor" required></div>
            </div>
          </div>

          <!-- <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Jumlah Bulan <span class="text-danger">*</span></label>

              <select class="select floating" name="jml_bulan" required>
                <option value="1">Bayar 1 Bulan </option>
                <option value="2">Bayar 2 Bulan </option>
                <option value="3">Bayar 3 Bulan </option>
                <option value="4">Bayar 4 Bulan </option>
                <option value="5">Bayar 5 Bulan </option>
                <option value="6">Bayar 6 Bulan </option>
                <option value="7">Bayar 7 Bulan </option>
                <option value="8">Bayar 8 Bulan </option>
                <option value="9">Bayar 9 Bulan </option>
                <option value="10">Bayar 10 Bulan </option>
                <option value="11">Bayar 11 Bulan </option>
                <option value="12">Bayar 12 Bulan </option>
              </select>
            </div>
          </div> -->
          <div class="col-sm-6">
            <div class="form-group">
              <label class="control-label">Jenis Simpanan <span class="text-danger">*</span></label>
              <?php $jenis = App\JenisSimpanan::find(1); ?>
              <select class="select floating" name="jenis_simpanan" required>
                <option value="{{$jenis->id}}" selected> {{$jenis->name}} </option>

              </select>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Jenis Mutasi <span class="text-danger">*</span></label>
              <?php $mutasi = ['Kredit']; ?>
              <select class="select floating" name="mutasi" required>
                @foreach($mutasi as $mut)
                    <option value="{{$mut}}"> {{$mut}} </option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Kas / Bank <span class="text-danger">*</span></label>
              <?php $banks = ['Kas','Bank']; ?>
              <select class="select floating" name="kasbank" required>
                <option value="">Pilih</option>
                @foreach($banks as $bank)
                    <option value="{{$bank}}"> {{$bank}} </option>
                @endforeach
              </select>
            </div>
          </div>
          <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
    var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
    angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
    }
    function rupiah(){ var target_donasi = document.getElementById("nominal").value; var rupiah = convertToRupiah(target_donasi);
    document.getElementById("nominal").value = rupiah; }
    </script>
    <script> function convertToRupiah (objek) {
     separator = ".";
     a = objek.value;
     b = a.replace(/[^Rp \d]/g,"");
     c = "";
     panjang = b.length;
     j = 0; for (i = panjang; i > 0; i--) {
       j = j + 1;
      if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + separator + c;
       } else {
         c = b.substr(i-1,1) + c;
       }
     } objek.value = c;
    }
     </script>
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label">Jumlah Simpanan<span class="text-danger">*</span></label>
              <input type="text" class="form-control form-control-rounded" onkeyup="convertToRupiah(this)" name="nominal" id="nominal" placeholder="Rp 0" required>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label class="control-label">Keterangan Tambahan<span class="text-danger">*</span></label>
              <textarea class="form-control" type="text" name="ket" required></textarea>
            </div>
          </div>
        <div class="m-t-20 text-center">
          <button class="btn btn-primary">P R O S E S</button>
        </div>
        </div>

    </form>
  </div>
</div>
  <!-- <div class="col-sm-2 col-xs-12"><br>
  <a href="{{url('/administrator/export-data-simpanan')}}" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export1').submit();"/></a>
              </div> -->
</div>

<div class="row">
  <!-- <form class="form" action="{{url('/administrator/export-data-simpanan')}}" method="post" id="export1">
    @csrf
    <input type="hidden" name="no_anggota" value="{{$nomor}}"/>
    <input type="hidden" name="dari" value="{{$from}}"/>
    <input type="hidden" name="sampai" value="{{$to}}"/>
    <input type="hidden" name="jenis_simpanan" value="{{$jenissim}}"/>
    <input type="hidden" name="mutasi" value="{{$mutasis}}"/>
    <input type="hidden" name="export" value="1"/>
  </form> -->

<!-- <div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table datatable">
      <thead>
        <tr>
          <th>No.</th>
          <th>Tgl. Transaksi</th>
          <th>No.Angota</th>
          <th>Nama</th>
          <th>Jenis Simpanan</th>
          <th>Nominal</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;
        $kredit = 0;
        $debet = 0;?>
        @foreach($simpanan as $us)
        <?php if ($us->mutasi =='Kredit') {
            $kredit+=$us->nominal;
        } else {
            $debet+=$us->nominal;
        } ?>
        <tr>
          <td>{{$no++}}.</td>
          <td>{{date('d-m-Y', strtotime($us->tgl_setor))}}</td>
          <td>{{$us->no_anggota}}</td>
          <td>{{$us->name}}</td>

          <td>{{$us->jenisSimpanan->name}}</td>
          <td>Rp {{number_format($us->nominal,0,",",".")}}</td>

        </tr>
        @endforeach
        @if(count($simpanan) < 1)
        <tr>
          <td colspan="9" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div> -->

<!-- <div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">

  <tr>
    <td>Total Simpanan Pokok</td>
    <td>: Rp {{number_format($kredit,0,",",".")}}</td>
  </tr>
</table>

</div> -->
<!-- <form class="" action="{{url('/administrator/data-simpanan')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-2"><br><br>
      <input class="form-control rounded btn-success" type="submit" value="Upload">
  </div>
</form> -->
</div>

    </div>


    <div id="add_employee" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Tambah Simpanan</h4>
          </div>

      </div>
    </div>

    <div id="edit_employee" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Edit Data Pegawai</h4>
          </div>
          <div class="modal-body">
            <form class="m-b-30" action="{{url('/admin/edit-data-pegawai/')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">NIP<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="nip" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="name" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Email <span class="text-danger">*</span></label>
                    <input class="form-control" type="email" name="email" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Telpon <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="telp" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Alamat <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="alamat" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tempat Lahir <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="tmp_lahir" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tanggal Lahir <span class="text-danger">*</span></label>
                    <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir" value=""></div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                    <?php $jns_klm = [
                      'Laki-Laki',
                      'Perempuan',
                    ]; ?>
                    <select class="select" name="jenkel" required>
                      <option value="">Pilih Jenis Kelamin</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Agama <span class="text-danger">*</span></label>
                    <?php $agamas = [
                      'Islam',
                      'Kristen',
                      'Protestan',
                      'Hindu',
                      'Budha',
                    ]; ?>
                    <select class="select" name="agama" required>
                      <option value="">Pilih Agama</option>
                    </select>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Status Kawin <span class="text-danger">*</span></label>
                    <?php $status_kwn = [
                      'Sudah Menikah',
                      'Belum Menikah',
                    ]; ?>
                    <select class="select" name="stts_kawin" required>
                      <option value="">Pilih Status Kawin</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jabatan <span class="text-danger">*</span></label>
                    <select class="select" name="jabatan" required>
                      <option value="">Pilih Jabatan</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Satuan Kerja <span class="text-danger">*</span></label>
                    <select class="select" name="satuan_kerja_id" required>
                      <option value="">Pilih Satuan Kerja</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tanggal Masuk <span class="text-danger">*</span></label>
                    <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tmt_masuk" value="" required></div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Golongan <span class="text-danger">*</span></label>
                    <select class="select" name="golongan" required>
                      <option value="">Pilih Golongan</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Status <span class="text-danger">*</span></label>
                    <select class="select" name="stts_pegawai" required>
                      <?php $status = [
                        'CPNS',
                        'PNS Tetap',
                        'Honorer',
                      ]; ?>
                      <option value="">Pilih Status</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto <span class="text-danger">*</span></label>
                    <input class="form-control" type="file" name="foto">
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="control-label">Hak Akses <span class="text-danger">*</span></label>
                    <select class="select" name="type" required>
                      <?php $akses = [
                        'admin',
                        'kadinas',
                        'pegawai'
                      ]; ?>
                      <option value="">Pilih Hak Akses</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jabatan Atasan<span class="text-danger">*</span></label>
                    <select class="select" name="jabatan_penilai_id" required>
                      <option value="">Pilih Jabatan</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Satuan Kerja Atasan<span class="text-danger">*</span></label>
                    <select class="select" name="satuan_penilai_id" required>
                      <option value="">Pilih Satuan Kerja</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="m-t-20 text-center">
                <button class="btn btn-primary">Save Changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div id="delete_employee" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Hapus Data Pegawai</h4>
          </div>
          <form action="{{url('/admin/hapus-data-pegawai/')}}" method="post">
            <div class="modal-body card-box">
              <p>Apakah yakin ingin di Hapus ???</p>
              <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                <button type="submit" class="btn btn-danger">Delete</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
