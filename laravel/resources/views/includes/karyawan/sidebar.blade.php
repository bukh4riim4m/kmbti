<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
  @if($dashboard =='dashboard')<li class="active">@else <li>@endif
  <!-- <li> -->
    <a href="{{url('/karyawan/home')}}">DASHBOARD KARYAWAN</a>
  </li>
  @if($dashboard =='dataAnggota')<li class="active">@else <li>@endif
    <a href="{{url('/karyawan/data-anggota')}}">DATA ANGGOTA</a>
  </li>
  @if($dashboard =='akumulasi')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>AKUMULASI BELANJA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/karyawan/akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Laporan Akumulasi</a></li>
      <!-- <li><a href="{{url('/karyawan/saldo-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Saldo Akumulasi</a></li> -->
    </ul>
  </li>
  @if($dashboard =='profil')<li class="active">@else <li>@endif
    <a href="{{url('/karyawan/profil')}}">PROFIL</a>
  </li>
  @if($dashboard =='gantiPassword')<li class="active">@else <li>@endif
    <a href="{{url('/karyawan/ganti-password')}}">GANTI PASSWORD</a>
  </li>
  <li>
    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">KELUAR</a>
  </li>
</ul>
</div>
    </div>
</div>
