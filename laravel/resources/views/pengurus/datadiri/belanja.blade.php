@extends('layouts.pengurus.app')
@section('content')
            <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-xs-12">
							<h4 class="page-title">Shopping</h4>
						</div>

					</div>
					<div class="row filter-row">
            <form class="m-b-30" action="" method="post">
              @csrf
              <input type="hidden" name="action" value="cari">
              <div class="col-sm-4 col-xs-6">
  							<div class="form-group form-focus">
  								<label class="control-label">Kode Produks</label>
  								<input type="text" class="form-control floating" name="kode" value="{{$kode}}"/>
  							</div>
  						</div>
              <div class="col-sm-4 col-xs-6">
  							<div class="form-group form-focus">
  								<label class="control-label">Nama Produk</label>
  								<input type="text" class="form-control floating" name="name" value="{{$name}}"/>
  							</div>
  						</div>
  						<div class="col-sm-4 col-xs-12">
  							<button type="submit" class="btn btn-success btn-block"> TAMPILKAN </button>
  						</div>
            </form>
          </div>
					<div class="row staff-grid-row">
            @foreach($produks as $produk)
						<div class="col-md-4 col-sm-4 col-xs-6 col-lg-3">
							<div class="profile-widget">
								<div class="profile-imges">
									<a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%"></a>
								</div>
                <p><h6 class="user-name m-t-10 m-b-0 text-left">Kode : {{$produk->kode}}</h6></p>
								<h6 class="user-name m-t-10 m-b-0 text-left">{{$produk->name}}</h6>
								<h6 class="user-name m-t-10 m-b-0 text-left">Rp {{number_format($produk->harga,0,",",".")}}</h6>
                <h6 class="user-name m-t-10 m-b-0 text-left">Berat : {{$produk->berat}} Gram</h6>
                <h6 class="user-name m-t-10 m-b-0 text-left">Stok : {{$produk->stok}}</h6>
                <div class="small text-muted"></div><hr>
                <form class="" action="{{route('pengurus-belanja')}}" method="post" id="beli{{$produk->id}}">
                  @csrf
                  <input type="hidden" name="action" value="beli">
                  <input type="hidden" name="ids" value="{{$produk->id}}">
                  <a href="#" onclick="event.preventDefault();
                                document.getElementById('beli{{$produk->id}}').submit();" class="btn btn-primary btn-sm m-t-10">BELI</a>
                  <a href="#" data-toggle="modal" data-target="#detail{{$produk->id}}" class="btn btn-default btn-sm m-t-10">DETAIL</a>
                </form>
							</div>
						</div>
            @endforeach
            @foreach($produks as $edituser)
            <div id="detail{{$edituser->id}}" class="modal custom-modal fade" role="dialog">
      				<div class="modal-dialog">
      					<div class="modal-content modal-md">
      						<div class="modal-header">
      							<h4 class="modal-title">Detail Produk</h4>
      						</div>
      						<div class="modal-body card-box col-md-12">
                      <div class="col-md-6">
                        <p><img src="{{url('laravel/public/gambars/'.$edituser->gambar)}}" width="100%"></p>
                      </div>
                      <div class="col-md-6">
                        <p><strong>Kode :</strong> {{$edituser->kode}}</p>
                        <p><strong>Nama :</strong> {{$edituser->name}}</p>
                        <p><strong>Harga :</strong> {{number_format($edituser->harga,0,",",".")}}</p>
                        <p><strong>Stok :</strong> {{$edituser->stok}}</p>
                        <p><strong>Keterangan :</strong> </p>
                        <p>{{$edituser->keterangan}}</p>
                      </div>

                      <div class="m-t-20"><hr>
                        <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                        <button type="submit" class="btn btn-primary">BELI</button>
                      </div>
      						</div>

      					</div>
      				</div>
      			</div>
            @endforeach
					</div>
      </div>

    </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
