@extends('layouts.pengurus.app')
@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">
					<div class="row">
						<div class="col-xs-4">
							<h4 class="page-title">Produk</h4>
						</div>
            <div class="col-xs-8 text-right m-b-30">
              <div class="view-icons">
              </div>
            </div>
					</div>
					<div class="row filter-row">
            <form class="m-b-30" action="{{url('/pengurus/produk')}}" method="post">
              @csrf
              <input type="hidden" name="action" value="cari">
              <div class="col-sm-4 col-xs-6">
  							<div class="form-group form-focus">
  								<label class="control-label">Kode Produks</label>
  								<input type="text" class="form-control floating" name="kode" value="{{$kode}}"/>
  							</div>
  						</div>
              <div class="col-sm-4 col-xs-6">
  							<div class="form-group form-focus">
  								<label class="control-label">Nama Produk</label>
  								<input type="text" class="form-control floating" name="name" value="{{$name}}"/>
  							</div>
  						</div>
  						<div class="col-sm-4 col-xs-12">
  							<button type="submit" class="btn btn-success btn-block"> TAMPILKAN </button>
  						</div>
            </form>
          </div>
					<div class="row staff-grid-row">
            @foreach($produks as $produk)
						<div class="col-md-4 col-sm-4 col-xs-6 col-lg-3">
							<div class="profile-widget">
								<div class="profile-imges">
									<a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%"></a>
								</div>
                <p><h6 class="user-name m-t-10 m-b-0 text-left">Kode : {{$produk->kode}}</h6></p>
								<h6 class="user-name m-t-10 m-b-0 text-left"><strong>{{$produk->name}}</strong></h6>
								<h6 class="user-name m-t-10 m-b-0 text-left">Rp {{number_format($produk->harga,0,",",".")}}</h6>
                <h6 class="user-name m-t-10 m-b-0 text-left">Berat : {{$produk->berat}} Gram</h6>
                <h6 class="user-name m-t-10 m-b-0 text-left">Stok : {{$produk->stok}}</h6>
                <div class="small text-muted"></div><hr>
                <a href="#" data-toggle="modal" data-target="#detail{{$produk->id}}" class="btn btn-success btn-sm m-t-10">Keterangan</a>
							</div>
						</div>
            @endforeach
            @foreach($produks as $edituser)
            <div id="detail{{$edituser->id}}" class="modal custom-modal fade" role="dialog">
      				<div class="modal-dialog">
      					<div class="modal-content modal-md">
      						<div class="modal-header">
      							<h4 class="modal-title">Keterangan</h4>
      						</div>
      						<div class="modal-body card-box">
                    <p>{{$edituser->keterangan}}</p>
                    <div class="m-t-20"> <a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
      						</div>
      					</div>
      				</div>
      			</div>
					</div>
          @endforeach
      </div>
    </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
